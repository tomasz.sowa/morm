#!/bin/sh

CURRENT_DIR=$(pwd)
GLOBAL_WORKING_DIR=$(realpath ${CURRENT_DIR}/..)
QTCREATOR_DIR=${CURRENT_DIR}/.qtcreator

# ----------

# make sure the current directory is correct
if [ ! -d ${CURRENT_DIR}/.templates ] ; then
	echo "this script should be called by make: make qtcreator (one level up)"
	exit 1
fi

qtcreatorini=~/.config/QtProject/QtCreator.ini
qtprofiles=~/.config/QtProject/qtcreator/profiles.xml

if [ ! -f "$qtcreatorini" ] ; then
	echo "I cannot find a $qtcreatorini file"
	echo "this file is needed to take an EnvironmentId parameter"
	echo "if QTCreator was recently installed make sure you have opened it at least once"
	exit 2
fi

if [ ! -f "$qtprofiles" ] ; then
	echo "I cannot find a $qtprofiles file"
	echo "this file is needed to take a default profile id parameter"
	echo "if QTCreator was recently installed make sure you have opened it at least once"
	exit 3
fi

qtenvironmentid=`grep -F 'Settings\EnvironmentId' "$qtcreatorini" | grep -Eo '\{[a-zA-Z0-9-]+\}'`
qtdefaultprofileid=`grep -A 1 -F '<variable>Profile.Default</variable>' "$qtprofiles" | tail -n 1 | grep -Eo '\{[a-zA-Z0-9-]+\}'`

if [ -z "$qtenvironmentid" ] ; then
	echo "I cannot read a Settings\EnvironmentId parameter from $qtcreatorini file"
	exit 4
fi

if [ -z "$qtdefaultprofileid" ] ; then
	echo "I cannot read a Profile.Default parameter from $qtprofiles file"
	exit 5
fi

# change / to \/ in paths
CURRENT_DIR_FOR_SED=$(echo ${CURRENT_DIR} | sed 's/\//\\\//g')
GLOBAL_WORKING_DIR_FOR_SED=$(echo ${GLOBAL_WORKING_DIR} | sed 's/\//\\\//g')

was_qtcreator_dir=0

if [ -d "${QTCREATOR_DIR}" ] ; then
	was_qtcreator_dir=1
else
	echo "Your QTCreator EnvironmentId is: $qtenvironmentid"
	echo "Your QTCreator default profile id is: $qtdefaultprofileid"
	mkdir -p "${QTCREATOR_DIR}"
fi

if [ -d "${QTCREATOR_DIR}" ] ; then

	if [ -d ${CURRENT_DIR}/.templates/.qtcreator ] ; then
		cd ${CURRENT_DIR}/.templates/.qtcreator
		
		if [ $? -eq 0 ] ; then

			for i in * ; do
				if [ ! -f "${QTCREATOR_DIR}/$i" ] ; then
					cat "$i" | \
						sed -e "s/%%%CURRENT_DIR%%%/${CURRENT_DIR_FOR_SED}/g" \
							-e "s/%%%GLOBAL_WORKING_DIR%%%/${GLOBAL_WORKING_DIR_FOR_SED}/g" \
							-e "s/%%%QT_ENVIRONMENT_ID%%%/${qtenvironmentid}/g" \
							-e "s/%%%QT_DEFAULT_PROFILE_ID%%%/${qtdefaultprofileid}/g" > \
						"${QTCREATOR_DIR}/$i"
				fi
			done

			if [ $was_qtcreator_dir -eq 0 ] ; then
				echo "QTCreator files generated to ${QTCREATOR_DIR} directory"
				echo "select an 'open project...' option and find a ${QTCREATOR_DIR}/*.creator file"
			fi

		fi
	fi


	find ${QTCREATOR_DIR} -name "*.files" | while read projectfiles ; do
	
		cd ${CURRENT_DIR}/src
		
		if [ $? -eq 0 ] ; then
			find ../src -type f \( -iname "*.cpp" -o -iname "*.h" \) | sort > ${projectfiles}
			echo "file ${projectfiles} has been updated"
		fi

	done

fi


