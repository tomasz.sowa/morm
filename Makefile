

export CXX
export CXXFLAGS
export AR


all: src


src: FORCE
	$(MAKE) -C src


samples: FORCE
	$(MAKE) -C src
	$(MAKE) -C samples


samples-gcc11: FORCE
	env CXX=g++11 CXXFLAGS="-Wl,-rpath=/usr/local/lib/gcc11/ -Wall -pedantic -O0 -g -std=c++20 -fmax-errors=1 -I../src -I../../pikotools/src -I/usr/local/include" $(MAKE) -C src
	env CXX=g++11 CXXFLAGS="-Wl,-rpath=/usr/local/lib/gcc11/ -Wall -pedantic -O0 -g -std=c++20 -fmax-errors=1 -I../src -I../../pikotools/src -I/usr/local/include" $(MAKE) -C samples


clean: FORCE
	$(MAKE) -C src clean
	$(MAKE) -C samples clean


cleanall: clean
	$(MAKE) -C ../pikotools clean


depend: FORCE
	$(MAKE) -C src depend
	$(MAKE) -C samples depend


clangd: FORCE
	@.templates/install_clangd.sh


qtcreator: clangd
	@.templates/install_qtcreator.sh


FORCE:

