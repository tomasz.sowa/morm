/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_samples_attachment2
#define headerfile_morm_samples_attachment2

#include <string>
#include "morm.h"
#include "type.h"
#include "language.h"


namespace morm
{
namespace samples
{

/*
CREATE TABLE public.attachment2 (
    id bigserial,
    person_id bigint,
    name varchar(64),
    content text,
    some_flags bool,
    created_date timestamp with time zone,
    language_id bigint,

    primary key(id)
);
*/

// copied from Attachment and changed the table name
class Attachment2 : public morm::Model
{
public:

	long id;
	long person_id;
	std::wstring name;
	std::string content;
	std::vector<Type> types;
	bool some_flags;
	pt::Date created_date;
	Language language;


	void fields()
	{
		field(L"id", id, FT::no_insertable | FT::no_updatable | FT::primary_key);
		field(L"person_id", person_id);
		field(L"name", name);
		field(L"content", content);
		field(L"attachment_id", L"types", types, FT::foreign_key_in_child);
		field(L"some_flags", some_flags);
		field(L"created_date", created_date);
		field(L"language_id", L"language", language, FT::foreign_key);
	}

	void table()
	{
		table_name(L"public", L"attachment2");
	}

	void after_select()
	{
		if( has_primary_key_set )
		{
			morm::Finder<Type> finder(model_connector);
			types = finder.select().where().eq(L"attachment_id", id).get_vector();
		}
	}

	void after_insert()
	{
		get_last_sequence_for_primary_key(L"public.attachment2_id_seq", id);
	}


};



}
}


#endif

