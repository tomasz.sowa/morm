/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2019-2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_samples_language
#define headerfile_morm_samples_language

#include <string>
#include "morm.h"


namespace morm
{
namespace samples
{

/*
CREATE TABLE public.language (
    id bigserial,
    english_name varchar(64),
    local_name varchar(64),
    code_str varchar(4),
    code_int int,

    primary key(id)
);
*/


class Language : public morm::Model
{
public:

	long id;
	std::wstring english_name;
	std::wstring local_name;
	std::wstring code_str;
	int code_int;


	void fields()
	{
		field(L"id", id, FT::no_insertable | FT::no_updatable | FT::primary_key);
		field(L"english_name", english_name);
		field(L"local_name", local_name);
		field(L"code_str", code_str);
		field(L"code_int", code_int);
	}

	void table()
	{
		table_name(L"public", L"language");
	}

	void after_insert()
	{
		get_last_sequence_for_primary_key(L"public.language_id_seq", id);
	}


};



}
}


#endif

