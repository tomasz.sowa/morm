/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2019-2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "mainoptions/mainoptionsparser.h"
#include "sample01.h"


namespace morm
{
namespace samples
{


void print_syntax()
{
	std::cout << "mormsample" << std::endl;

	std::cout << "options:" << std::endl;

	std::cout << " -h " << std::endl;
	std::cout << " --help - print this help and exit" << std::endl;
	std::cout << std::endl;

}


}
}


int main(int argc, const char ** argv)
{
pt::Space args, options;

	options.add(L"c", 1); // one argument - config file
	options.add(L"config", 1);

	pt::MainOptionsParser main_parser;
	pt::MainOptionsParser::Status args_status = main_parser.parse(argc, argv, args, options);

	if( args_status != pt::MainOptionsParser::status_ok )
	{
		std::cout << "Syntax error in arguments" << std::endl;
		return 1;
	}

	if( args.has_key(L"h") || args.has_key(L"help") )
	{
		morm::samples::print_syntax();
		return 0;
	}


	morm::ModelConnector model_connector;
	morm::JSONConnector json_connector;
	morm::PostgreSQLConnector postgresql_connector;

	std::wstring db_name = L"morm_test";
	std::wstring db_user = L"morm_test";
	std::wstring db_pass = L"morm_test";
	/*
	 * PostgreSQL
	 *
	 * you can create a new user with:
	 * create user morm_test LOGIN ENCRYPTED PASSWORD 'morm_test';
	 *
	 * and a new database with:
	 * create database morm_test owner morm_test;
	 *
	 */
	pt::Log log;
	pt::FileLog file_log;
	pt::WTextStream log_buffer;

	file_log.init(L"log.txt", true, 4, true);
	log.set_log_buffer(&log_buffer);
	log.set_file_log(&file_log);



	postgresql_connector.set_conn_param(db_name, db_user, db_pass);
	postgresql_connector.set_logger(log);
	postgresql_connector.set_log_queries(true);
	postgresql_connector.wait_for_connection();

	model_connector.set_flat_connector(json_connector);
	model_connector.set_db_connector(postgresql_connector);
	model_connector.set_logger(log);


	morm::samples::Sample01 sample_01;
	sample_01.set_connector(model_connector);
	sample_01.make();

}



