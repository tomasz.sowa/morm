/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2019-2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_samples_sample01
#define headerfile_morm_samples_sample01

#include <ctime>
#include "basesample.h"
#include "person.h"
#include "language.h"
#include "attachment.h"


namespace morm
{
namespace samples
{

class Sample01 : public BaseSample
{
public:


void make()
{
	// delete all datas
	// delete from attachment; delete from attachment2; delete from language; delete from person; delete from types;

	// select * from person; select * from attachment; select * from attachment2 ; select * from types; select * from language;

	std::string str;


	person.set_connector(model_connector);
	load_defaults(person);

	//std::wstring sss = L"some text put dynamically";
	//person.set_field_value_generic(L"email", L"email", sss);



	//person.insert();
	//person.update();
	//person.save();
	//person.remove();
	//person.to_text(str, true, true);


	morm::Finder<Person> finder(model_connector);
	Person p = finder.select().where().eq(L"id", 210).get();
	p.to_text(str, true, true);



	//std::list<Person> plist = finder.use_table_prefix(false).select().where().eq(L"id", 120).get_list();


//	Person p = finder.prepare_to_select().use_table_prefix(true).raw("select person.id as \"person.id\", person.first_name as \"person.first_name\", person.last_name as \"person.last_name\", person.email as \"person.email\", "
//			"language.id as \"language.id\", language.english_name as \"language.english_name\", language.local_name as \"language.local_name\", language.code_str as \"language.code_str\", language.code_int as \"language.code_int\", "
//			"attachment.id as \"attachment.id\", attachment.person_id as \"attachment.person_id\", attachment.name as \"attachment.name\", attachment.content as \"attachment.content\", attachment.some_flags as \"attachment.some_flags\", attachment.created_date as \"attachment.created_date\","
//			"language2.id as \"language2.id\", language2.english_name as \"language2.english_name\", language2.local_name as \"language2.local_name\", language2.code_str as \"language2.code_str\", language2.code_int as \"language2.code_int\""
//			"FROM public.person AS person "
//			"LEFT JOIN public.language AS language ON person.language_id = language.id "
//			"LEFT JOIN public.attachment AS attachment ON person.id = attachment.person_id "
//			"LEFT JOIN public.language AS language2 ON attachment.language_id = language2.id "
//			"where person.id=120").get();

	//std::cout << "--------------------------------" << std::endl;
	//p.remove();
	//std::cout << "--------------------------------" << std::endl;

	//str += "--------\n";



//	for(Person & person : plist)
//	{
//		person.to_text(str, false, true);
//		str += "\n--------\n";
//	}

	std::cout << str << std::endl;

}



private:

	Person person;



	static void load_defaults(Person & person)
	{
		person.id = 0;
		person.first_name = L"MyFirstName";
		person.last_name = L"MyLastName";
		person.email = L"myemail@mydomain.ltd";
		//person.set_save_mode(Model::DO_NOTHING_ON_SAVE);
		person.set_save_mode(Model::DO_INSERT_ON_SAVE);
		//person.set_save_mode(Model::DO_UPDATE_ON_SAVE);

		person.language.id = 0;
		person.language.english_name = L"english";
		person.language.local_name = L"polish";
		person.language.code_str = L"en";
		person.language.code_int = 200;
		//person.language.set_save_mode(Model::DO_NOTHING_ON_SAVE);
		person.language.set_save_mode(Model::DO_INSERT_ON_SAVE);
		//person.language.set_save_mode(Model::DO_UPDATE_ON_SAVE);

		std::time_t t = std::time(0);

		Type type;
		type.id = 0;
		type.set_save_mode(Model::DO_INSERT_ON_SAVE);

		person.attachment2.id = 40;
		person.attachment2.person_id = person.id;
		person.attachment2.created_date.FromTime(t);
		person.attachment2.name = L"attachment name";
		person.attachment2.content = "long binary content";
		person.attachment2.some_flags = true;
		person.attachment2.set_save_mode(Model::DO_INSERT_ON_SAVE);
//		//person.attachment.set_save_mode(Model::DO_UPDATE_ON_SAVE);

		person.attachment2.language.id = 86;
		person.attachment2.language.english_name = L"attachment language";
		person.attachment2.language.local_name = L"attachment local name";
		person.attachment2.language.code_str = L"loen";
		person.attachment2.language.code_int = 300;
		person.attachment2.language.set_save_mode(Model::DO_INSERT_ON_SAVE);
		person.attachment2.language.set_has_primary_key_set(true);

		type.name = L"abcde - fghi";
		person.attachment2.types.push_back(type);

		type.name = L"second type";
		person.attachment2.types.push_back(type);

		Attachment attachment;
		attachment.id = 0;
		attachment.person_id = person.id;
		attachment.created_date.FromTime(t);
		attachment.name = L"list attachment 1";
		attachment.content = "list attachment content";
		attachment.some_flags = true;
		attachment.set_save_mode(Model::DO_INSERT_ON_SAVE);
		//attachment.set_save_mode(Model::DO_UPDATE_ON_SAVE);

		attachment.language.english_name = L"Language dla attachment 1";
		person.attachments.push_back(attachment);

		type.name = L"Typ dla attachment 1 - 1";
		person.attachments.back().types.push_back(type);

		type.name = L"Typ dla attachment 1 - 2";
		person.attachments.back().types.push_back(type);

		attachment.name = L"list attachment 2";
		attachment.language.english_name = L"Language dla attachment 2";
		person.attachments.push_back(attachment);

		type.name = L"Typ dla attachment 2 - 1";
		person.attachments.back().types.push_back(type);

		type.name = L"Typ dla attachment 2 - 2";
		person.attachments.back().types.push_back(type);

		attachment.name = L"list attachment 3";
		attachment.language.english_name = L"Language dla attachment 3";
		person.attachments.push_back(attachment);

		type.name = L"Typ dla attachment 3 - 1";
		person.attachments.back().types.push_back(type);

		type.name = L"Typ dla attachment 3 - 2";
		person.attachments.back().types.push_back(type);

		type.name = L"Typ dla attachment 3 - 3";
		person.attachments.back().types.push_back(type);

		//type.name = L"Typik";
		//person.attachment.types.push_back(type);

}




};


}
}

#endif

