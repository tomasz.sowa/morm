/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "baseexpression.h"
#include "morm_types.h"
#include "model.h"
#include "utf8/utf8.h"


namespace morm
{


BaseExpression::BaseExpression()
{
	/*
	 * may it would be better for the ModelConnector/JSONConnector/PostgreSQLConnector to provide one scratch buffer?
	 */
	scratch_buffer = &scratch_buffer_local;
	clear();
}


BaseExpression::~BaseExpression()
{
}


void BaseExpression::clear()
{
	out_stream = nullptr;
	is_first_field = false;
	work_mode = 0;
	output_type = 0;
	use_prefix = false;
}


void BaseExpression::set_work_mode(int work_mode)
{
	this->work_mode = work_mode;
}


int BaseExpression::get_work_mode()
{
	return work_mode;
}


void BaseExpression::set_output_type(int output_type)
{
	this->output_type = output_type;
}


int BaseExpression::get_output_type()
{
	return output_type;
}



pt::Stream * BaseExpression::get_text_stream()
{
	return out_stream;
}


void BaseExpression::set_text_stream(pt::Stream * out_stream)
{
	this->out_stream = out_stream;
}


void BaseExpression::allow_to_use_prefix(bool use_prefix)
{
	this->use_prefix = use_prefix;
}


bool BaseExpression::get_allow_to_use_prefix()
{
	return use_prefix;
}



void BaseExpression::generate_from_model(pt::Stream & stream, Model & model)
{
	FT field_type = FT::default_type;
	generate_from_model(stream, model, field_type);
}


void BaseExpression::generate_from_model(pt::Stream & stream, Model & model, const FT & field_type)
{
	this->out_stream = &stream;
	generate_from_model(model, field_type);
	this->out_stream = nullptr;
}


void BaseExpression::generate_from_model(Model & model, const FT & field_type)
{
	if( out_stream )
	{
		if( should_field_model_be_generated_as_null(model.get_has_primary_key_set(), field_type) )
		{
			put_null_value();
		}
		else
		{
			before_generate_from_model();
			dump_additional_info(model);
			model.fields();
			add_additional_columns(model);
			after_generate_from_model();
		}
	}
}


void BaseExpression::dump_additional_info(Model & model)
{
	if( model.model_env && model.model_env->dump_mode )
	{
		field(L"model_save_mode",     model.save_mode,           FT::no_insertable | FT::no_updatable | FT::no_fetchable, model.model_env);
		field(L"has_primary_key_set", model.has_primary_key_set, FT::no_insertable | FT::no_updatable | FT::no_fetchable, model.model_env);
	}
}


void BaseExpression::add_additional_columns(Model & model)
{
}


void BaseExpression::before_generate_from_model()
{
	is_first_field = true;
}


void BaseExpression::after_generate_from_model()
{
}


bool BaseExpression::can_field_be_generated(const FT &)
{
	return true;
}


bool BaseExpression::can_field_model_be_generated(bool has_model_primary_key, const FT & field_type)
{
	return true;
}


bool BaseExpression::can_field_list_be_generated(const FT &)
{
	return true;
}



bool BaseExpression::should_field_model_be_generated_as_null(bool has_model_primary_key, const FT & field_type)
{
	return false;
}

void BaseExpression::field_before()
{
	if( !is_first_field )
	{
		// put a separator between fields
	}
}


void BaseExpression::field_after()
{
	is_first_field = false;
}



void BaseExpression::put_field_name_and_table_if_needed(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env)
{
	if( !field_type.is_raw_field_name() && use_prefix && model_env )
	{
		if( !is_empty_field(model_env->table2_name) )
		{
			put_table_with_index_and_field(model_env->table2_name, model_env->table2_index, field_name, field_type);
		}
		else
		{
			put_table_with_index_and_field(model_env->table_name, model_env->table_index, field_name, field_type);
		}
	}
	else
	{
		put_field_name(field_name, field_type, model_env);
	}
}



void BaseExpression::put_field_name(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_raw_field_name() )
	{
		(*out_stream) << field_name;
	}
	else
	{
		before_field_name();
		esc(field_name, *out_stream, FT::default_type, nullptr); /* do not use provided field_type here - it would use e.g. binary mode if it was set, similar don't use model_env */
		after_field_name();
	}
}


void BaseExpression::put_field_closing_name(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env)
{
	put_field_name(field_name, field_type, model_env);
}


void BaseExpression::put_value_list_opening_index(size_t index, const FT & field_type)
{
}


void BaseExpression::put_value_list_closing_index(size_t index, const FT & field_type)
{
}


void BaseExpression::save_foreign_key(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env)
{
	pt::TextStream str;
	pt::Stream * old_out_stream = out_stream;

	out_stream = &str;
	put_field_name(field_name, field_type, model_env);
	out_stream = old_out_stream;

	if( model_env && model_env->finder_helper )
	{
		model_env->finder_helper->foreign_keys.emplace_back();
		std::string & key_str = model_env->finder_helper->foreign_keys.back();
		str.to_str(key_str, false);
	}
}



void BaseExpression::schema_table_separator()
{
}


void BaseExpression::table_field_separator()
{
}


void BaseExpression::alias_names_separator()
{
}


void BaseExpression::before_schema_name()
{
}


void BaseExpression::after_schema_name()
{
}


void BaseExpression::before_table_name()
{
}


void BaseExpression::after_table_name()
{
}


void BaseExpression::before_field_name()
{
}


void BaseExpression::after_field_name()
{
}


void BaseExpression::before_alias_name()
{
}


void BaseExpression::after_alias_name()
{
}


void BaseExpression::before_field_value(const std::wstring &, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::before_field_value(const std::string &, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(const std::wstring &, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(const std::string &, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}


void BaseExpression::before_field_value(const wchar_t *, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(const wchar_t *, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}


void BaseExpression::before_field_value(const char *, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(const char *, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}


void BaseExpression::before_field_value(wchar_t, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(wchar_t, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}


void BaseExpression::before_field_value(char, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}


void BaseExpression::after_field_value(char, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}

void BaseExpression::before_field_value(const pt::Date &, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}

void BaseExpression::after_field_value(const pt::Date &, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}

void BaseExpression::before_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	before_field_value_string(field_type, model_env);
}

void BaseExpression::after_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	after_field_value_string(field_type, model_env);
}

void BaseExpression::before_field_value_generic()
{
}

void BaseExpression::after_field_value_generic()
{
}



void BaseExpression::put_name_value_separator()
{
	(*out_stream) << ',';
}


char BaseExpression::char_to_hex_part(char c)
{
	if( c < 10 )
		return c + '0';

	return c - 10 + 'A';
}


void BaseExpression::char_to_hex(char c, pt::Stream & stream)
{
	stream << char_to_hex_part(((unsigned char)c) >> 4);
	stream << char_to_hex_part(((unsigned char)c) & 0x0f);
}


void BaseExpression::char_to_hex(char32_t c, pt::Stream & stream)
{
	unsigned int z = static_cast<unsigned int>(c);

	char_to_hex((char)(unsigned char)(z >> 24), stream);
	char_to_hex((char)(unsigned char)(z >> 16), stream);
	char_to_hex((char)(unsigned char)(z >> 8), stream);
	char_to_hex((char)(unsigned char)(z), stream);
}



/*
 * return true if the val character was escaped and put (or ignored) to the stream
 *
 */
bool BaseExpression::esc_char(char val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	return esc_char((char32_t)(unsigned char)val, stream, field_type, model_env);
}


/*
 * return true if the val character was escaped and put (or ignored) to the stream
 *
 *
 */
bool BaseExpression::esc_char(wchar_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	return esc_char((char32_t)val, stream, field_type, model_env);
}


/*
 * return true if the val character was escaped and put (or ignored) to the stream
 *
 * in most cases you have to provide your own esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env) method
 *
 */
bool BaseExpression::esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	return false;
}


void BaseExpression::esc(char val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_binary() || field_type.is_hexadecimal() )
	{
		char_to_hex(val, stream);
	}
	else
	{
		if( !esc_char(val, stream, field_type, model_env) )
		{
			stream << val;
		}
	}
}


void BaseExpression::esc(unsigned char val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc(static_cast<char>(val), stream, field_type, model_env);
}


void BaseExpression::esc(wchar_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc(static_cast<char32_t>(val), stream, field_type, model_env);
}


void BaseExpression::esc(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_binary() || field_type.is_hexadecimal() )
	{
		char_to_hex(val, stream);
	}
	else
	{
		if( field_type.use_utf8() )
		{
			if( !esc_char(val, stream, field_type, model_env) )
			{
				stream << val;
			}
		}
		else
		{
			char val_char = (char)(unsigned char)val;

			if( !esc_char(val_char, stream, field_type, model_env) )
			{
				stream << val_char;
			}
		}
	}
}


void BaseExpression::esc_numeric_string(const char * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_numeric_string_generic(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_numeric_string(const wchar_t * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_numeric_string_generic(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_hex_string(const char * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_hex_string_generic(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_hex_string(const wchar_t * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_hex_string_generic(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_bin_string(const char * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_hex_string(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_bin_string(const wchar_t * val, bool has_known_length, size_t len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_hex_string(val, has_known_length, len, stream, field_type, model_env);
}


void BaseExpression::esc_ordinary_string(const char * val, size_t val_len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	pt::utf8_to_output_function(val, val_len, [&](int c){
		esc(static_cast<char32_t>(c), stream, field_type, model_env);
	});
}


void BaseExpression::esc_ordinary_string(const char * val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	pt::utf8_to_output_function(val, [&](int c){
		esc(static_cast<char32_t>(c), stream, field_type, model_env);
	});
}


void BaseExpression::esc_ordinary_string(const wchar_t * val, size_t val_len, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	pt::wide_to_output_function(val, val_len, [&](int c){
		esc(static_cast<char32_t>(c), stream, field_type, model_env);
	});
}


void BaseExpression::esc_ordinary_string(const wchar_t * val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	pt::wide_to_output_function(val, [&](int c){
		esc(static_cast<char32_t>(c), stream, field_type, model_env);
	});
}


void BaseExpression::esc(const std::wstring & val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_string(val.c_str(), true, val.size(), stream, field_type, model_env);
}


void BaseExpression::esc(const wchar_t * val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_string(val, false, 0, stream, field_type, model_env);
}


void BaseExpression::esc(const std::string & val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_string(val.c_str(), true, val.size(), stream, field_type, model_env);
}


void BaseExpression::esc(const char * val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	esc_string(val, false, 0, stream, field_type, model_env);
}


void BaseExpression::esc(bool val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( val )
		stream << "true";
	else
		stream << "false";
}


void BaseExpression::esc(short val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(unsigned short val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(int val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(unsigned int val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(long val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(unsigned long val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(long long val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(unsigned long long val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(float val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(double val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(long double val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	stream << val;
}


void BaseExpression::esc(const pt::Date & date, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_date_only() )
	{
		date.SerializeYearMonthDay(stream, false);
	}
	else
	if( field_type.is_time_only() )
	{
		date.SerializeHourMinSec(stream);
	}
	else
	{
		if( field_type.is_no_time_zone() )
		{
			date.Serialize(stream);
		}
		else
		{
			date.SerializeISO(stream);
		}
	}
}


void BaseExpression::esc(const pt::Space & space, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	pt::WTextStream tmp_stream;
	bool pretty_print = field_type.is_pretty_print();

	if( field_type.is_space() )
		space.serialize_to_space_stream(tmp_stream, pretty_print);
	else
		space.serialize_to_json_stream(tmp_stream, pretty_print);

	esc(tmp_stream, stream, field_type, model_env);
}


void BaseExpression::esc(const pt::Stream & val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( val.is_char_stream() )
	{
		if( field_type.is_hexadecimal() || field_type.is_binary() )
		{
			for(size_t i=0 ; i<val.size() ; ++i )
			{
				esc(val.get_char(i), stream, field_type, model_env);
			}
		}
		else
		{
			// from utf8 to utf8 or from utf8 to wide
			pt::utf8_to_output_function_by_index(val, [&](int z) {
				esc(static_cast<char32_t>(z), stream, field_type, model_env);
			});
		}
	}
	else
	if( val.is_wchar_stream() )
	{
		// from wide to wide or from wide to utf8
		pt::wide_to_output_function_by_index(val, [&](int z) {
			esc(static_cast<char32_t>(z), stream, field_type, model_env);
		});
	}
}


//void BaseExpression::put_type(char val, 				pt::Stream & stream)
//{
//	stream << "char";
//}
//
//void BaseExpression::put_type(unsigned char val, 		pt::Stream & stream)
//{
//	stream << "unsigned char";
//}
//
//
//void BaseExpression::put_type(const std::wstring & val, pt::Stream & stream)
//{
//	stream << "text";
//}
//
//void BaseExpression::put_type(const wchar_t * val, 		pt::Stream & stream)
//{
//	stream << "text";
//}
//
//
//void BaseExpression::put_type(const std::string & val, 	pt::Stream & stream)
//{
//	stream << "text";
//}
//
//void BaseExpression::put_type(const char * val, 			pt::Stream & stream)
//{
//	stream << "text";
//}
//
//
//void BaseExpression::put_type(bool val, 				pt::Stream & stream)
//{
//	stream << "boolean";
//}
//
//void BaseExpression::put_type(short val, 				pt::Stream & stream)
//{
//	stream << "short integer";
//}
//
//void BaseExpression::put_type(unsigned short val, 		pt::Stream & stream)
//{
//	stream << "unsigned short integer";
//}
//
//void BaseExpression::put_type(int val, 					pt::Stream & stream)
//{
//	stream << "integer";
//}
//
//void BaseExpression::put_type(unsigned int val, 		pt::Stream & stream)
//{
//	stream << "unsigned integer";
//}
//
//void BaseExpression::put_type(long val, 				pt::Stream & stream)
//{
//	stream << "long integer";
//}
//
//void BaseExpression::put_type(unsigned long val, 		pt::Stream & stream)
//{
//	stream << "unsigned long integer";
//}
//
//void BaseExpression::put_type(long long val, 			pt::Stream & stream)
//{
//	stream << "very long integer";
//}
//
//void BaseExpression::put_type(unsigned long long val, 	pt::Stream & stream)
//{
//	stream << "unsigned very long integer";
//}
//
//void BaseExpression::put_type(float val, 				pt::Stream & stream)
//{
//	stream << "float";
//}
//
//void BaseExpression::put_type(double val, 				pt::Stream & stream)
//{
//	stream << "double";
//}
//
//void BaseExpression::put_type(long double val, 			pt::Stream & stream)
//{
//	stream << "long double";
//}
//
////void BaseExpression::put_type(void* val, 				pt::Stream & stream)
////{
////}
//
//
//void BaseExpression::put_type(const pt::Date & date, 	pt::Stream & stream)
//{
//	stream << "date";
//}
//
//void BaseExpression::put_type(const Model & model, 		pt::Stream & stream)
//{
//	stream << "object";
//}


void BaseExpression::before_field_value_string(const FT & field_type, ModelEnv * model_env)
{
}

void BaseExpression::after_field_value_string(const FT & field_type, ModelEnv * model_env)
{
}


/*
 * schema_name can be a null pointer or just pointing to an empty string
 * in such a case we do not put the schema name
 */
void BaseExpression::put_schema_table(const wchar_t * schema_name, const wchar_t * table_name)
{
	if( out_stream )
	{
		bool has_schema = false;

		if( !is_empty_field(schema_name) )
		{
			has_schema = true;
			before_schema_name();
			esc(schema_name, *out_stream);
			after_schema_name();
		}

		if( !is_empty_field(table_name) )
		{
			if( has_schema )
				schema_table_separator();

			before_table_name();
			esc(table_name, *out_stream);
			after_table_name();
		}
	}
}


/*
 * schema_name can be empty - in such a case we do not put the schema name
 */
void BaseExpression::put_schema_table(const pt::WTextStream & schema_name, const pt::WTextStream & table_name)
{
	if( out_stream )
	{
		bool has_schema = false;

		if( !schema_name.empty() )
		{
			has_schema = true;
			before_schema_name();
			esc(schema_name, *out_stream);
			after_schema_name();
		}

		if( !table_name.empty() )
		{
			if( has_schema )
				schema_table_separator();

			before_table_name();
			esc(table_name, *out_stream);
			after_table_name();
		}
	}
}


void BaseExpression::put_table(const wchar_t * table_name)
{
	if( out_stream )
	{
		before_table_name();
		esc(table_name, *out_stream);
		after_table_name();
	}
}


void BaseExpression::put_table(const pt::WTextStream & table_name)
{
	if( out_stream )
	{
		before_table_name();
		esc(table_name, *out_stream);
		after_table_name();
	}
}


void BaseExpression::put_table_with_index(const wchar_t * table_name, int index)
{
	if( out_stream )
	{
		before_table_name();
		esc(table_name, *out_stream);

		if( index > 1 )
		{
			(*out_stream) << index;
		}

		after_table_name();
	}
}


void BaseExpression::put_table_with_index(const pt::WTextStream & table_name, int index)
{
	if( out_stream )
	{
		before_table_name();
		esc(table_name, *out_stream);

		if( index > 1 )
		{
			(*out_stream) << index;
		}

		after_table_name();
	}
}


void BaseExpression::put_table_with_index_and_field(const wchar_t * table_name, int index, const wchar_t * field_name, const FT & field_type)
{
	if( out_stream )
	{
		put_table_with_index(table_name, index);
		table_field_separator();
		put_field_name(field_name, field_type, nullptr);
	}
}

void BaseExpression::put_table_with_index_and_field(const pt::WTextStream & table_name, int index, const wchar_t * field_name, const FT & field_type)
{
	if( out_stream )
	{
		put_table_with_index(table_name, index);
		table_field_separator();
		put_field_name(field_name, field_type, nullptr);
	}
}


void BaseExpression::put_table_and_field(const wchar_t * table_name, const wchar_t * field_name, const FT & field_type)
{
	if( out_stream )
	{
		put_table(table_name);
		table_field_separator();
		put_field_name(field_name, field_type, nullptr);
	}
}


void BaseExpression::put_table_and_field(const pt::WTextStream & table_name, const wchar_t * field_name, const FT & field_type)
{
	if( out_stream )
	{
		put_table(table_name);
		table_field_separator();
		put_field_name(field_name, field_type, nullptr);
	}
}


void BaseExpression::put_alias(const pt::WTextStream & alias_name, int index)
{
	if( out_stream )
	{
		before_alias_name();
		esc(alias_name, *out_stream);

		if( index > 1 )
		{
			(*out_stream) << index;
		}

		after_alias_name();
	}
}


void BaseExpression::put_alias(const pt::WTextStream & alias_name_prefix, int index, const wchar_t * alias_name_postfix)
{
	if( out_stream )
	{
		before_alias_name();
		esc(alias_name_prefix, *out_stream);

		if( index > 1 )
		{
			(*out_stream) << index;
		}

		alias_names_separator();
		esc(alias_name_postfix, *out_stream);
		after_alias_name();
	}
}



void BaseExpression::put_string(const char * str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	put_string_generic(str, field_type, add_quotes, model_env);
}


void BaseExpression::put_string(const wchar_t * str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	put_string_generic(str, field_type, add_quotes, model_env);
}


void BaseExpression::put_string(const std::string & str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	put_string_generic(str, field_type, add_quotes, model_env);
}


void BaseExpression::put_string(const std::wstring & str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	put_string_generic(str, field_type, add_quotes, model_env);
}


void BaseExpression::put_stream(const pt::Stream & str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	put_string_generic(str, field_type, add_quotes, model_env);
}


void BaseExpression::schema_table_to_stream(pt::Stream & stream, const wchar_t * schema_name, const wchar_t * table_name)
{
	this->out_stream = &stream;
	put_schema_table(schema_name, table_name);
	this->out_stream = nullptr;
}


void BaseExpression::schema_table_to_stream(pt::Stream & stream, const pt::WTextStream & schema_name, const pt::WTextStream & table_name)
{
	this->out_stream = &stream;
	put_schema_table(schema_name, table_name);
	this->out_stream = nullptr;
}


void BaseExpression::table_to_stream(pt::Stream & stream, const wchar_t * table_name)
{
	this->out_stream = &stream;
	put_table(table_name);
	this->out_stream = nullptr;
}


void BaseExpression::table_to_stream(pt::Stream & stream, const pt::WTextStream & table_name)
{
	this->out_stream = &stream;
	put_table(table_name);
	this->out_stream = nullptr;
}


void BaseExpression::table_with_index_to_stream(pt::Stream & stream, const wchar_t * table_name, int index)
{
	this->out_stream = &stream;
	put_table_with_index(table_name, index);
	this->out_stream = nullptr;
}


void BaseExpression::table_with_index_to_stream(pt::Stream & stream, const pt::WTextStream & table_name, int index)
{
	this->out_stream = &stream;
	put_table_with_index(table_name, index);
	this->out_stream = nullptr;
}


void BaseExpression::table_with_index_and_field_to_stream(pt::Stream & stream, const wchar_t * table_name, int index, const wchar_t * field_name, const FT & field_type)
{
	this->out_stream = &stream;
	put_table_with_index_and_field(table_name, index, field_name, field_type);
	this->out_stream = nullptr;
}


void BaseExpression::table_with_index_and_field_to_stream(pt::Stream & stream, const pt::WTextStream & table_name, int index, const wchar_t * field_name, const FT & field_type)
{
	this->out_stream = &stream;
	put_table_with_index_and_field(table_name, index, field_name, field_type);
	this->out_stream = nullptr;
}


void BaseExpression::table_and_field_to_stream(pt::Stream & stream, const wchar_t * table_name, const wchar_t * field_name, const FT & field_type)
{
	this->out_stream = &stream;
	put_table_and_field(table_name, field_name, field_type);
	this->out_stream = nullptr;
}


void BaseExpression::table_and_field_to_stream(pt::Stream & stream, const pt::WTextStream & table_name, const wchar_t * field_name, const FT & field_type)
{
	this->out_stream = &stream;
	put_table_and_field(table_name, field_name, field_type);
	this->out_stream = nullptr;
}


void BaseExpression::alias_to_stream(pt::Stream & stream, const pt::WTextStream & alias_name, int index)
{
	this->out_stream = &stream;
	put_alias(alias_name, index);
	this->out_stream = nullptr;
}


void BaseExpression::alias_to_stream(pt::Stream & stream, const pt::WTextStream & alias_name_prefix, int index, const wchar_t * alias_name_postfix)
{
	this->out_stream = &stream;
	put_alias(alias_name_prefix, index, alias_name_postfix);
	this->out_stream = nullptr;
}




void BaseExpression::string_to_stream(pt::Stream & stream, const char * str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	this->out_stream = &stream;
	put_string(str, field_type, add_quotes, model_env);
	this->out_stream = nullptr;
}


void BaseExpression::string_to_stream(pt::Stream & stream, const wchar_t * str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	this->out_stream = &stream;
	put_string(str, field_type, add_quotes, model_env);
	this->out_stream = nullptr;
}


void BaseExpression::string_to_stream(pt::Stream & stream, const std::string & str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	this->out_stream = &stream;
	put_string(str, field_type, add_quotes, model_env);
	this->out_stream = nullptr;
}


void BaseExpression::string_to_stream(pt::Stream & stream, const std::wstring & str, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	this->out_stream = &stream;
	put_string(str, field_type, add_quotes, model_env);
	this->out_stream = nullptr;
}


void BaseExpression::stream_to_stream(pt::Stream & stream_out, const pt::Stream & stream_in, const FT & field_type, bool add_quotes, ModelEnv * model_env)
{
	this->out_stream = &stream_out;
	put_stream(stream_in, field_type, add_quotes, model_env);
	this->out_stream = nullptr;
}


bool BaseExpression::is_empty_field(const wchar_t * value)
{
	return (!value || *value == '\0');
}


}

