/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_baseobjectwrapper
#define headerfile_morm_src_baseobjectwrapper



namespace morm
{


class BaseObjectWrapper
{
public:

	BaseObjectWrapper()
	{
		auto_remove = true;
		reference_counter = 1;
	}

	virtual ~BaseObjectWrapper()
	{
	}

	virtual bool should_be_auto_removed()
	{
		return auto_remove;
	}

	virtual void increment_reference_counter()
	{
		reference_counter += 1;
	}

	virtual void decrement_reference_counter()
	{
		if( reference_counter > 0 )
			reference_counter -= 1;
	}

	virtual size_t get_reference_counter()
	{
		return reference_counter;
	}


protected:

	bool auto_remove;
	size_t reference_counter;


private:

	/*
	 * don't copy these wrappers
	 */
	BaseObjectWrapper(const BaseObjectWrapper & wrapper)
	{
		operator=(wrapper);
	}

	BaseObjectWrapper(BaseObjectWrapper && wrapper)
	{
		auto_remove = wrapper.auto_remove;
		reference_counter = wrapper.reference_counter;
		wrapper.auto_remove = false;
		wrapper.reference_counter = 0;
	}

	BaseObjectWrapper & operator=(const BaseObjectWrapper & wrapper)
	{
		auto_remove = wrapper.auto_remove;
		reference_counter = 1;
		return *this;
	}

};

}

#endif
