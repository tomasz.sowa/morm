/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "clearer.h"
#include "model.h"


namespace morm
{


Clearer::Clearer()
{
}


Clearer::~Clearer()
{
}


void Clearer::clear_value(char & field_value, const FT & field_type)
{
	field_value = 0;
}


void Clearer::clear_value(unsigned char & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(wchar_t & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(std::wstring & field_value, const FT & field_type)
{
	if( field_type.is_numeric() )
	{
		field_value = L"0";
	}
	else
	{
		field_value.clear();
	}
}


void Clearer::clear_value(std::string & field_value, const FT & field_type)
{
	if( field_type.is_numeric() )
	{
		field_value = "0";
	}
	else
	{
		field_value.clear();
	}
}


void Clearer::clear_value(bool & field_value, const FT & field_type)
{
	field_value = false;
}

void Clearer::clear_value(short & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(unsigned short & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(int & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(unsigned int & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(long & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(unsigned long & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(long long & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(unsigned long long & field_value, const FT & field_type)
{
	field_value = 0;
}

void Clearer::clear_value(float & field_value, const FT & field_type)
{
	field_value = 0.0f;
}

void Clearer::clear_value(double & field_value, const FT & field_type)
{
	field_value = 0.0;
}

void Clearer::clear_value(long double & field_value, const FT & field_type)
{
	field_value = 0.0;
}

void Clearer::clear_value(pt::Date & field_value, const FT & field_type)
{
	field_value.Clear();
}

void Clearer::clear_value(pt::Space & field_value, const FT & field_type)
{
	field_value.clear();
}

void Clearer::clear_value(pt::Stream & field_value, const FT & field_type)
{
	field_value.clear();
}

void Clearer::clear_model(Model & field_value, const FT & field_type)
{
	field_value.clear();
}







}

