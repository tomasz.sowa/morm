/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_clearer
#define headerfile_morm_src_clearer

#include <string>
#include "date/date.h"
#include "space/space.h"
#include "ft.h"


namespace morm
{
class Model;


class Clearer
{
public:

	Clearer();
	virtual ~Clearer();

	virtual void clear_value(char & field_value, const FT & field_type);
	virtual void clear_value(unsigned char & field_value, const FT & field_type);
	virtual void clear_value(wchar_t & field_value, const FT & field_type);
	virtual void clear_value(std::wstring & field_value, const FT & field_type);
	virtual void clear_value(std::string & field_value, const FT & field_type);
	virtual void clear_value(bool & field_value, const FT & field_type);
	virtual void clear_value(short & field_value, const FT & field_type);
	virtual void clear_value(unsigned short & field_value, const FT & field_type);
	virtual void clear_value(int & field_value, const FT & field_type);
	virtual void clear_value(unsigned int & field_value, const FT & field_type);
	virtual void clear_value(long & field_value, const FT & field_type);
	virtual void clear_value(unsigned long & field_value, const FT & field_type);
	virtual void clear_value(long long & field_value, const FT & field_type);
	virtual void clear_value(unsigned long long & field_value, const FT & field_type);
	virtual void clear_value(float & field_value, const FT & field_type);
	virtual void clear_value(double & field_value, const FT & field_type);
	virtual void clear_value(long double & field_value, const FT & field_type);
	virtual void clear_value(pt::Date & field_value, const FT & field_type);
	virtual void clear_value(pt::Space & field_value, const FT & field_type);
	virtual void clear_value(pt::Stream & field_value, const FT & field_type);

	virtual void clear_model(Model & field_value, const FT & field_type);

	template<typename ModelContainer, typename ModelContainerType, typename IsContainerByValueRenameMe>
	void clear_container(ModelContainer & container, ModelContainerType * model_container_type, IsContainerByValueRenameMe * foo, const FT & field_type)
	{
		if constexpr (std::is_base_of<Model, ModelContainerType>())
		{
			if constexpr (!std::is_base_of<Model, IsContainerByValueRenameMe>())
			{
//				for(auto * item : container)
//				{
// IMPROVEME we need to rethink how to handle pointers
//					delete item;
//					item = nullptr;
//				}
			}
		}

		container.clear();
	}



	// give here containers?

protected:




};

}

#endif


