/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2023-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "csvconnector.h"
#include "csvexpression.h"


namespace morm
{


CSVConnector::CSVConnector()
{
}


void CSVConnector::allocate_default_expression()
{
	deallocate_expression();
	flat_expression = new CSVExpression();
	expression_allocated = true;

}


void CSVConnector::to_text(pt::Stream & stream, Model & model, Export exp)
{
	allocate_default_expression_if_needed();

	if( flat_expression )
	{
		flat_expression->clear();

		if( exp.is_export_headers() )
		{
			flat_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS);
			flat_expression->set_output_type(MORM_OUTPUT_TYPE_FIELDS_RECURSIVE);
		}
		else
		{
			flat_expression->set_work_mode(MORM_WORK_MODE_MODEL_VALUES);
		}

		flat_expression->allow_to_use_prefix(false);
		flat_expression->generate_from_model(stream, model);
	}
}


}
