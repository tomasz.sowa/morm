/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <cstdlib>
#include <memory>
#include "space/spaceparser.h"
#include "dbconnector.h"
#include "dbexpression.h"
#include "model.h"
#include "utf8/utf8.h"
#include "convert/convert.h"


namespace morm
{

DbConnector::DbConnector()
{
	db_expression = nullptr;
	expression_allocated = false;
	log = nullptr;
	log_queries = false;
	transaction_index = 0;
	transaction_group = 0;
}

DbConnector::~DbConnector()
{
	deallocate_expression();
}


void DbConnector::set_logger(pt::Log * log)
{
	this->log = log;
}


void DbConnector::set_logger(pt::Log & log)
{
	this->log = &log;
}


void DbConnector::set_log_queries(bool log_queries)
{
	this->log_queries = log_queries;
}


bool DbConnector::query(const pt::Stream & stream)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());
	return query(stream, *query_result_ptr);
}


bool DbConnector::query(const std::string & query_str)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());
	return query(query_str, *query_result_ptr);
}


bool DbConnector::query(const char * query_str)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());
	return query(query_str, *query_result_ptr);
}



bool DbConnector::query(const pt::Stream & stream, QueryResult & query_result)
{
	std::string query_str;
	stream.to_str(query_str);

	return query(query_str, query_result);
}


bool DbConnector::query(const std::string & query_str, QueryResult & query_result)
{
	return query(query_str.c_str(), query_result);
}


bool DbConnector::query(const char * query_str, QueryResult & query_result)
{
	// do the query
	return false;
}



bool DbConnector::query_select(const char * query_str, QueryResult & query_result)
{
	return query(query_str, query_result);
}

bool DbConnector::query_update(const char * query_str, QueryResult & query_result)
{
	return query(query_str, query_result);
}

bool DbConnector::query_insert(const char * query_str, QueryResult & query_result)
{
	return query(query_str, query_result);
}

bool DbConnector::query_remove(const char * query_str, QueryResult & query_result)
{
	return query(query_str, query_result);
}

bool DbConnector::query_declare_cursor(const char * query_str, QueryResult & query_result)
{
	return query(query_str, query_result);
}



bool DbConnector::query_select(const pt::Stream & stream, QueryResult & query_result)
{
	return query(stream, query_result);
}

bool DbConnector::query_update(const pt::Stream & stream, QueryResult & query_result)
{
	return query(stream, query_result);
}

bool DbConnector::query_insert(const pt::Stream & stream, QueryResult & query_result)
{
	return query(stream, query_result);
}

bool DbConnector::query_remove(const pt::Stream & stream, QueryResult & query_result)
{
	return query(stream, query_result);
}

bool DbConnector::query_declare_cursor(const pt::Stream & stream, QueryResult & query_result)
{
	return query(stream, query_result);
}



bool DbConnector::begin()
{
	bool status = false;

	if( transaction_index > 0 )
	{
		pt::TextStream str;
		str << "SAVEPOINT savepoint_" << transaction_index;
		status = DbConnector::query(str);
	}
	else
	{
		status = DbConnector::query("BEGIN");
	}

	if( status )
	{
		transaction_index += 1;
	}
	else
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: I cannot start a transaction" << pt::Log::logend;
		}
	}

	return status;
}


bool DbConnector::begin_if_needed()
{
	bool status = true;

	if( transaction_index == 0 )
	{
		status = begin();
	}

	return status;
}


bool DbConnector::rollback()
{
	return rollback(transaction_index);
}


bool DbConnector::commit()
{
	return commit(transaction_index);
}



bool DbConnector::rollback_one_transaction(size_t index)
{
	bool status = false;

	if( index > 1 )
	{
		pt::TextStream str;
		str << "ROLLBACK TO SAVEPOINT savepoint_" << (index - 1);
		status = DbConnector::query(str);
		transaction_index = index - 1; // decrement it even if rollback failed
	}
	else
	if( index == 1 )
	{
		status = DbConnector::query("ROLLBACK");
		transaction_index = 0;
		transaction_group += 1;
	}

	return status;
}




bool DbConnector::rollback(size_t index)
{
	bool status = false;

	if( index == 0 )
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: there is no a transaction with zero index - skipping rollback";
		}
	}
	else
	if( index > transaction_index )
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: transaction";

			if( index > 1 )
				(*log) << " for savepoint_" << (index-1);

			(*log) << " does not exist - skipping rollback" << pt::Log::logend;
		}
	}
	else
	{
		status = true;

		for(size_t i = transaction_index ; i >= index ; --i)
		{
			if( !rollback_one_transaction(i) )
			{
				/*
				 * return false if at least one rollback failed
				 */
				status = false;
			}
		}
	}

	return status;
}


bool DbConnector::commit_one_transaction(size_t index)
{
	bool status = false;

	if( index > 1 )
	{
		pt::TextStream str;
		str << "RELEASE SAVEPOINT savepoint_" << (index - 1);
		status = DbConnector::query(str);
		transaction_index = index - 1;
	}
	else
	if( index == 1 )
	{
		status = DbConnector::query("COMMIT");
		transaction_index = 0;
		transaction_group += 1;
	}

	return status;
}


bool DbConnector::commit(size_t index)
{
	bool status = false;

	if( index == 0 )
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: there is no a transaction with zero index - skipping commit";
		}
	}
	else
	if( index > transaction_index )
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: transaction";

			if( index > 1 )
				(*log) << " for savepoint_" << (index-1);

			(*log) << " does not exist - skipping commit" << pt::Log::logend;
		}
	}
	else
	{
		status = true;

		for(size_t i = transaction_index ; i >= index ; --i)
		{
			if( !commit_one_transaction(i) )
			{
				/*
				 * return false if at least one commit failed
				 */
				status = false;
			}
		}
	}

	return status;
}





size_t DbConnector::get_transaction_index()
{
	return transaction_index;
}


size_t DbConnector::get_transaction_group()
{
	return transaction_group;
}




DbExpression * DbConnector::get_expression()
{
	allocate_default_expression_if_needed();
	return db_expression;
}


void DbConnector::generate_select_columns(pt::Stream & stream, Model & model)
{
	allocate_default_expression_if_needed();

	if( db_expression )
	{
		db_expression->clear();
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_SELECT_COLUMNS);
		db_expression->allow_to_use_prefix(true);
		db_expression->generate_from_model(stream, model);
	}
}


void DbConnector::generate_insert_query(pt::Stream & stream, Model & model)
{
	allocate_default_expression_if_needed();

	if( db_expression )
	{
		db_expression->clear();
		db_expression->allow_to_use_prefix(false);

		stream << "INSERT INTO ";
		db_expression->schema_table_to_stream(stream, model.model_env->schema_name, model.model_env->table_name);

		stream << " (";
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_INSERT);
		db_expression->generate_from_model(stream, model);

		stream << ") VALUES (";
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_VALUES);
		db_expression->generate_from_model(stream, model);
		stream << ")";
	}
}


void DbConnector::generate_update_query(pt::Stream & stream, Model & model)
{
	allocate_default_expression_if_needed();

	if( db_expression )
	{
		db_expression->clear();
		db_expression->allow_to_use_prefix(false);

		stream << "UPDATE ";
		db_expression->schema_table_to_stream(stream, model.model_env->schema_name, model.model_env->table_name);

		stream << " SET ";
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS_VALUES);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_UPDATE);
		db_expression->generate_from_model(stream, model);

		stream << " WHERE ";
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS_VALUES);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_PRIMARY_KEY);
		db_expression->generate_from_model(stream, model);
	}
}


void DbConnector::generate_remove_query(pt::Stream & stream, Model & model)
{
	allocate_default_expression_if_needed();

	if( db_expression )
	{
		db_expression->clear();
		db_expression->allow_to_use_prefix(false);

		stream << "DELETE FROM ";
		db_expression->schema_table_to_stream(stream, model.model_env->schema_name, model.model_env->table_name);

		stream << " WHERE ";
		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS_VALUES);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_PRIMARY_KEY);
		db_expression->generate_from_model(stream, model);
	}
}


bool DbConnector::insert(pt::Stream & stream, Model & model)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());

	generate_insert_query(stream, model);
	return query_insert(stream, *query_result_ptr);
}


bool DbConnector::update(pt::Stream & stream, Model & model)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());

	generate_update_query(stream, model);
	return query_update(stream, *query_result_ptr);
}


bool DbConnector::remove(pt::Stream & stream, Model & model)
{
	std::unique_ptr<QueryResult> query_result_ptr(create_query_result());

	generate_remove_query(stream, model);
	return query_remove(stream, *query_result_ptr);
}


void DbConnector::deallocate_expression()
{
	if( expression_allocated )
	{
		delete db_expression;
		db_expression = nullptr;
		expression_allocated = false;
	}
}



void DbConnector::allocate_default_expression_if_needed()
{
	if( !db_expression )
	{
		allocate_default_expression();
	}
}


unsigned int DbConnector::unescape_hex_char_part(char hex)
{
	if( hex>='0' && hex<='9' )
	{
		return hex - '0';
	}
	else
	if( hex>='a' && hex<='f' )
	{
		return hex - 'a' + 10;
	}
	else
	if( hex>='A' && hex<='F' )
	{
		return hex - 'A' + 10;
	}
	else
	{
		if( log )
		{
			(*log) << pt::Log::log2 << "Morm: incorrect character when reading a hex string, char code: " << (int)(unsigned char)hex;

			if( hex >= 32 )
			{
				(*log) << " '" << hex << "'";
			}

			(*log) << pt::Log::logend;
		}
	}

	return 0;
}



const char * DbConnector::unescape_hex_char(const char * str, size_t len, unsigned int & res)
{
	unsigned int c;
	res = 0;
	bool was_log_printed = false;

	for(size_t i = 0 ; i < len ; ++i)
	{
		if( *str != 0 )
		{
			c = unescape_hex_char_part(*str);
			str += 1;
		}
		else
		{
			c = 0;

			if( log && !was_log_printed )
			{
				(*log) << pt::Log::log2 << "Morm: the hex string ended unexpectedly, filling the part of the the last character with zeroes"
					   << pt::Log::logend;
				was_log_printed = true;
			}
		}

		res = (res << 4) | c;
	}

	return str;
}


void DbConnector::unescape_hex_char(const char * str, char & c)
{
	unsigned int res = 0;
	unescape_hex_char(str, sizeof(char) * 2, res);
	c = (char)res;
}


void DbConnector::unescape_hex_char(const char * str, char32_t & c)
{
	unsigned int res = 0;
	unescape_hex_char(str, sizeof(wchar_t) * 2, res);
	c = (char32_t)res;
}


void DbConnector::unescape_bin_char(const char * str, char & c)
{
	unescape_hex_char(str, c);
}


void DbConnector::unescape_bin_char(const char * str, char32_t & c)
{
	unescape_hex_char(str, c);
}


void DbConnector::unescape_hex_string(const char * str, std::string & out)
{
	unsigned int c = 0;

	while( *str != 0 )
	{
		str = unescape_hex_char(str, sizeof(char) * 2, c);
		out += (char)c;
	}
}


void DbConnector::unescape_hex_string(const char * str, std::wstring & out)
{
	unsigned int c = 0;

	while( *str != 0 )
	{
		str = unescape_hex_char(str, sizeof(wchar_t) * 2, c);
		out += (wchar_t)c;
	}
}


void DbConnector::unescape_hex_string(const char * str, pt::Stream & out)
{
	unsigned int c = 0;

	if( out.is_char_stream() )
	{
		while( *str != 0 )
		{
			str = unescape_hex_char(str, sizeof(char) * 2, c);
			out << (char)c;
		}
	}
	else
	if( out.is_wchar_stream() )
	{
		while( *str != 0 )
		{
			str = unescape_hex_char(str, sizeof(wchar_t) * 2, c);
			out << (wchar_t)c;
		}
	}
}


void DbConnector::unescape_bin_string(const char * str, std::string & out)
{
	unescape_hex_string(str, out);
}


void DbConnector::unescape_bin_string(const char * str, std::wstring & out)
{
	unescape_hex_string(str, out);
}


void DbConnector::unescape_bin_string(const char * str, pt::Stream & out)
{
	unescape_hex_string(str, out);
}


void DbConnector::get_value(const char * value_str, char & field_value, const FT & field_type)
{
	if( field_type.is_hexadecimal() )
	{
		unescape_hex_char(value_str, field_value);
	}
	else
	if( field_type.is_binary() )
	{
		unescape_bin_char(value_str, field_value);
	}
	else
	{
		field_value = *value_str;
	}
}



void DbConnector::get_value(const char * value_str, unsigned char & field_value, const FT & field_type)
{
	char tmp_char;
	get_value(value_str, tmp_char, field_type);
	field_value = static_cast<unsigned char>(tmp_char);
}


void DbConnector::get_value(const char * value_str, wchar_t & field_value, const FT & field_type)
{
	char32_t tmp;
	get_value(value_str, tmp, field_type);
	field_value = static_cast<wchar_t>(tmp);
}


void DbConnector::get_value(const char * value_str, char32_t & field_value, const FT & field_type)
{
	field_value = 0;

	if( field_type.is_hexadecimal() )
	{
		unescape_hex_char(value_str, field_value);
	}
	else
	if( field_type.is_binary() )
	{
		unescape_bin_char(value_str, field_value);
	}
	else
	{
		if( field_type.use_utf8() )
		{
			int value_int;
			bool is_correct;

			pt::utf8_to_int(value_str, value_int, is_correct);

			if( is_correct )
			{
				field_value = static_cast<char32_t>(value_int);
			}
			else
			{
				field_value = 0xFFFD; // U+FFFD "replacement character";
			}
		}
		else
		{
			field_value = static_cast<char32_t>((unsigned char)*value_str);
		}
	}
}


void DbConnector::get_value(const char * value_str, std::string & field_value, const FT & field_type)
{
	if( field_type.is_hexadecimal() )
	{
		unescape_hex_string(value_str, field_value);
	}
	else
	if( field_type.is_binary() )
	{
		unescape_bin_string(value_str, field_value);
	}
	else
	{
		field_value = value_str;
	}
}


void DbConnector::get_value(const char * value_str, std::string_view & field_value, const FT & field_type)
{
	field_value = value_str;
}


void DbConnector::get_value(const char * value_str, std::wstring & field_value, const FT & field_type)
{
	if( field_type.is_hexadecimal() )
	{
		unescape_hex_string(value_str, field_value);
	}
	else
	if( field_type.is_binary() )
	{
		unescape_bin_string(value_str, field_value);
	}
	else
	{
		if( field_type.use_utf8() )
		{
			pt::utf8_to_wide(value_str, field_value);
		}
		else
		{
			for(size_t i=0 ; value_str[i] != 0 ; ++i)
			{
				field_value += static_cast<wchar_t>((unsigned char)value_str[i]);
			}
		}
	}
}


void DbConnector::get_value(const char * value_str, bool & field_value, const FT & field_type)
{
	// IMPROVE ME
	// this 't' is locale dependent
	field_value = (value_str[0]=='t' || value_str[0]=='y' || value_str[0]=='1');
}


void DbConnector::get_value(const char * value_str, short & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = (short)pt::to_i(value_str, 10);
}


void DbConnector::get_value(const char * value_str, unsigned short & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = (unsigned short)pt::to_ui(value_str, 10);
}


void DbConnector::get_value(const char * value_str, int & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_i(value_str, 10);
}


void DbConnector::get_value(const char * value_str, unsigned int & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_ui(value_str, 10);
}


void DbConnector::get_value(const char * value_str, long & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_l(value_str, 10);
}


void DbConnector::get_value(const char * value_str, unsigned long & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_ul(value_str, 10);
}


void DbConnector::get_value(const char * value_str, long long & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_ll(value_str, 10);
}


void DbConnector::get_value(const char * value_str, unsigned long long & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = pt::to_ull(value_str, 10);
}


void DbConnector::get_value(const char * value_str, float & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = strtof(value_str, 0);
}


void DbConnector::get_value(const char * value_str, double & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = strtod(value_str, 0);
}


void DbConnector::get_value(const char * value_str, long double & field_value, const FT & field_type)
{
	// IMPROVE ME give some overflow checking
	field_value = strtold(value_str, 0);
}


void DbConnector::get_value(const char * value_str, pt::Date & field_value, const FT & field_type)
{
	// IMPROVE ME give some log if parsing failed

	if( field_type.is_date_only() )
	{
		field_value.ParseYearMonthDay(value_str);
	}
	else
	if( field_type.is_time_only() )
	{
		field_value.ParseHourMinSec(value_str);
	}
	else
	{
		field_value.Parse(value_str, !field_type.is_no_time_zone());
	}
}


void DbConnector::get_value(const char * value_str, pt::Space & field_value, const FT & field_type)
{
	field_value.clear();

	if( *value_str != '\0' )
	{
		pt::SpaceParser space_parser;

		if( field_type.is_space() )
		{
			if( space_parser.parse_space(value_str, field_value) != pt::SpaceParser::ok )
			{
				field_value.clear();

				if( log )
				{
					(*log) << pt::Log::log2 << "Morm: I cannot correctly parse the Space struct (space format) from the datebase"
						   << ", the raw string was: " << value_str << pt::Log::logend;
				}
			}
		}
		else
		{
			if( space_parser.parse_json(value_str, field_value) != pt::SpaceParser::ok )
			{
				field_value.clear();

				if( log )
				{
					(*log) << pt::Log::log2 << "Morm: I cannot correctly parse the Space struct (json format) from the datebase"
						   << ", the raw string was: " << value_str << pt::Log::logend;
				}
			}
		}
	}
}


void DbConnector::get_value(const char * value_str, pt::Stream & field_value, const FT & field_type)
{
	if( field_type.is_hexadecimal() )
	{
		unescape_hex_string(value_str, field_value);
	}
	else
	if( field_type.is_binary() )
	{
		unescape_bin_string(value_str, field_value);
	}
	else
	{
		if( field_type.use_utf8() )
		{
			pt::utf8_to_wide(value_str, field_value);
		}
		else
		{
			for(size_t i=0 ; value_str[i] != 0 ; ++i)
			{
				field_value << static_cast<wchar_t>((unsigned char)value_str[i]);
			}
		}
	}
}


const char * DbConnector::query_last_sequence(const wchar_t * sequence_table_name)
{
	return nullptr;
}



}
