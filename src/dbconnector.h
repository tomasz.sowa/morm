/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_dbconnector
#define headerfile_morm_src_dbconnector

#include "textstream/stream.h"
#include "log/log.h"
#include "queryresult.h"
#include "ft.h"


namespace morm
{
class Model;
class DbExpression;

class DbConnector
{

public:

	DbConnector();
	DbConnector(const DbConnector &) = delete;
	DbConnector(DbConnector &&) = delete;
	virtual ~DbConnector();

	virtual void set_logger(pt::Log * log);
	virtual void set_logger(pt::Log & log);

	virtual void set_log_queries(bool log_queries);

	DbExpression * get_expression();

	virtual void generate_select_columns(pt::Stream & stream, Model & model);
	virtual void generate_insert_query(pt::Stream & stream, Model & model);
	virtual void generate_update_query(pt::Stream & stream, Model & model);
	virtual void generate_remove_query(pt::Stream & stream, Model & model);

	virtual bool insert(pt::Stream & stream, Model & model);
	virtual bool update(pt::Stream & stream, Model & model);
	virtual bool remove(pt::Stream & stream, Model & model);

	virtual bool query(const pt::Stream & stream);
	virtual bool query(const std::string & query_str);
	virtual bool query(const char * query_str);

	virtual QueryResult * create_query_result() = 0;

	virtual bool query(const pt::Stream & stream, QueryResult & query_result);
	virtual bool query(const std::string & query_str, QueryResult & query_result);
	virtual bool query(const char * query_str, QueryResult & query_result);

	virtual bool query_select(const char * query_str, QueryResult & query_result);
	virtual bool query_update(const char * query_str, QueryResult & query_result);
	virtual bool query_insert(const char * query_str, QueryResult & query_result);
	virtual bool query_remove(const char * query_str, QueryResult & query_result);
	virtual bool query_declare_cursor(const char * query_str, QueryResult & query_result);

	virtual bool query_select(const pt::Stream & stream, QueryResult & query_result);
	virtual bool query_update(const pt::Stream & stream, QueryResult & query_result);
	virtual bool query_insert(const pt::Stream & stream, QueryResult & query_result);
	virtual bool query_remove(const pt::Stream & stream, QueryResult & query_result);
	virtual bool query_declare_cursor(const pt::Stream & stream, QueryResult & query_result);


	/*
	 * create a new transaction
	 * first transaction has index equal to one
	 */
	virtual bool begin();

	/*
	 * create a new transaction if there is no a transaction started yet
	 */
	virtual bool begin_if_needed();

	/*
	 * rollback or commit the last transaction
	 */
	virtual bool rollback();
	virtual bool commit();

	/*
	 * rollback or commit all transactions from the last one to the index
	 * (first transaction has index one, there is no a transaction with index zero)
	 *
	 */
	virtual bool rollback(size_t index);
	virtual bool commit(size_t index);

	/*
	 * return current transaction index (within a group)
	 * first transaction has index equal to one
	 * returns zero if there are no any transactions
	 */
	virtual size_t get_transaction_index();

	/*
	 * return current transaction group index
	 * a group index is incremented when you commit or rollback a transaction
	 * (if you are closing a nested transaction then the group index is not incremented)
	 *
	 */
	virtual size_t get_transaction_group();


	virtual void get_value(const char * value_str, char & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, unsigned char & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, wchar_t & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, char32_t & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, std::wstring & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, std::string & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, std::string_view & field_value, 	const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, bool & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, short & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, unsigned short & field_value, 	const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, int & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, unsigned int & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, long & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, unsigned long & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, long long & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, unsigned long long & field_value,	const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, float & field_value, 				const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, double & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, long double & field_value, 		const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, pt::Date & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, pt::Space & field_value, 			const FT & field_type = FT::default_type);
	virtual void get_value(const char * value_str, pt::Stream & field_value, 		const FT & field_type = FT::default_type);



	template<typename FieldValue>
	bool get_last_sequence(const wchar_t * sequence_table_name, FieldValue & field_value)
	{
		const char * val_str = query_last_sequence(sequence_table_name);

		if( val_str )
		{
			get_value(val_str, field_value);

			if( log && log_queries )
			{
				(*log) << pt::Log::log3 << "Morm: sequence value: " << field_value << pt::Log::logend;
			}

			return true;
		}

		return false;
	}


protected:

	DbExpression * db_expression;
	bool expression_allocated;
	pt::Log * log;
	bool log_queries;
	size_t transaction_index;
	size_t transaction_group;


	virtual void allocate_default_expression() = 0;
	virtual void allocate_default_expression_if_needed();
	virtual void deallocate_expression();

	virtual const char * query_last_sequence(const wchar_t * sequence_table_name);

	virtual void unescape_hex_char(const char * str, char & c);
	virtual void unescape_hex_char(const char * str, char32_t & c);

	virtual void unescape_bin_char(const char * str, char & c);
	virtual void unescape_bin_char(const char * str, char32_t & c);

	virtual void unescape_hex_string(const char * str, std::string & out);
	virtual void unescape_hex_string(const char * str, std::wstring & out);
	virtual void unescape_hex_string(const char * str, pt::Stream & out);

	virtual void unescape_bin_string(const char * str, std::string & out);
	virtual void unescape_bin_string(const char * str, std::wstring & out);
	virtual void unescape_bin_string(const char * str, pt::Stream & out);

	virtual bool rollback_one_transaction(size_t index);
	virtual bool commit_one_transaction(size_t index);

private:

	unsigned int unescape_hex_char_part(char hex);
	const char * unescape_hex_char(const char * str, size_t len, unsigned int & res);



};





}

#endif
