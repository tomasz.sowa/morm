/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_dbexpression
#define headerfile_morm_src_dbexpression

#include <vector>
#include "baseexpression.h"


namespace morm
{

class DbExpression : public BaseExpression
{
public:

	DbExpression();
	virtual ~DbExpression();


	constexpr static const char * COLUMN_ROWS_COUNTER_POSTFIX = "_autoadded_rows_counter";

	virtual void prepare_to_where_clause();

	virtual DbExpression & group_or(pt::Stream & stream);
	virtual DbExpression & group_and(pt::Stream & stream);
	virtual DbExpression & group_end(pt::Stream & stream);

	/*
	 * page_number starts from zero (it's a number of a page, not an offset)
	 */
	virtual DbExpression & page(pt::Stream & stream, size_t page_number, size_t page_size);

	template<typename FieldValue>
	void add_field_for_select(const wchar_t * new_column_expression, const wchar_t * new_column_name, FieldValue & field_value, const FT & field_type, ModelEnv * model_env)
	{
		std::wstring column_expression; // field() methods can be called recursively, so don't make it as class object

		column_expression = new_column_expression;
		column_expression += L" AS ";
		column_expression += new_column_name;

		field(column_expression.c_str(), field_value, field_type, model_env);
	}

	virtual void generate_rows_counter_column_name(ModelEnv & model_env, pt::Stream & str);

	virtual void prepare_declare_cursor_query(const pt::Stream & cursor_name, bool scroll_cursor, pt::Stream & out_stream);
	virtual void prepare_fetch_next_query(const pt::Stream & cursor_name, pt::Stream & out_stream);
	virtual void prepare_fetch_prior_query(const pt::Stream & cursor_name, pt::Stream & out_stream);
	virtual void prepare_fetch_first_query(const pt::Stream & cursor_name, pt::Stream & out_stream);
	virtual void prepare_fetch_last_query(const pt::Stream & cursor_name, pt::Stream & out_stream);
	virtual void prepare_fetch_absotule_query(const pt::Stream & cursor_name, long position, pt::Stream & out_stream);
	virtual void prepare_fetch_relative_query(const pt::Stream & cursor_name, long position, pt::Stream & out_stream);
	virtual void prepare_fetch_forward_count_query(const pt::Stream & cursor_name, size_t len, pt::Stream & out_stream);
	virtual void prepare_fetch_backward_count_query(const pt::Stream & cursor_name, size_t len, pt::Stream & out_stream);
	virtual void prepare_fetch_all_query(const pt::Stream & cursor_name, pt::Stream & out_stream);


protected:


	std::vector<int> conjunctions;

	bool can_field_be_generated(const FT & field_type) override;
	void field_before() override;
	void put_name_value_separator() override;

	void schema_table_separator() override;
	void table_field_separator() override;
	void alias_names_separator() override;

	void before_schema_name() override;
	void after_schema_name() override;

	void before_table_name() override;
	void after_table_name() override;

	void before_field_name() override;
	void after_field_name() override;

	void before_alias_name() override;
	void after_alias_name() override;

	void add_additional_columns(Model & model) override;

	void before_field_value_string(const FT & field_type, ModelEnv * model_env) override;
	void after_field_value_string(const FT & field_type, ModelEnv * model_env) override;

	virtual void add_rows_counter_column(Model & model);

};

}

#endif
