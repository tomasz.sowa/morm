/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2023, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWAExportType, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_export
#define headerfile_morm_src_export

namespace morm
{



/*
 * field types
 */
class Export
{
public:

	enum ExportType
	{
		default_type = 0,
		no_clear_stream = 1,
		dump_mode = 2,
		export_headers = 4,
	};

	/*
	 * type can be a superposition from ExportType values
	 */
	int type;



	Export()
	{
		type = 0;
	}

	Export(const Export & field_type)
	{
		type = field_type.type;
	}

	Export(ExportType type)
	{
		this->type = static_cast<int>(type);
	}

	Export(int type)
	{
		this->type = type;
	}

	Export & operator=(const Export & field_type)
	{
		type = field_type.type;
		return *this;
	}

	Export & operator=(ExportType type)
	{
		this->type = static_cast<int>(type);
		return *this;
	}

	Export & operator=(int type)
	{
		this->type = type;
		return *this;
	}

	bool is_flag_set(int flag_mask) const
	{
		return (type & flag_mask) != 0;
	}

	bool is_no_clear_stream() const
	{
		return is_flag_set(no_clear_stream);
	}

	bool is_dump_mode() const
	{
		return is_flag_set(dump_mode);
	}

	bool is_export_headers() const
	{
		return is_flag_set(export_headers);
	}

};

}

#endif

