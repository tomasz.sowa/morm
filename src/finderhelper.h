/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2019-2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_finderhelper
#define headerfile_morm_src_finderhelper

#include "queryresult.h"


namespace morm
{


class FinderHelper
{
public:

	pt::TextStream join_tables_str;

	std::map<std::wstring, int> join_tables_map;

	std::list<std::string> foreign_keys;



	FinderHelper()
	{
	}


	virtual ~FinderHelper()
	{
	}


	virtual void clear()
	{
		join_tables_str.clear();
		join_tables_map.clear();
		foreign_keys.clear();
	}


	virtual int add_join_table(const pt::WTextStream & table_name)
	{
		std::wstring table_name_str;
		table_name.to_str(table_name_str);

		return add_join_table(table_name_str);
	}


	virtual int add_join_table(const std::wstring & table_name)
	{
		auto res = join_tables_map.insert(std::make_pair(table_name, 1));

		if( !res.second )
		{
			res.first->second += 1;
		}

		return res.first->second;
	}


};

}

#endif
