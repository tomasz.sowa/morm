/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021-2023, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_ft
#define headerfile_morm_src_ft

namespace morm
{



/*
 * field types
 */
class FT
{
public:

	enum FieldType
	{
		default_type = 0,
		primary_key = 1,
		foreign_key = 2,
		foreign_key_in_child = 4,
		no_insertable = 8,
		no_updatable = 16,
		no_fetchable = 32, /* not supported yet */
		no_removable = 64,
		raw_field_name = 128,
		dont_use_utf8 = 256, /* used only with wchar_t and std::wstring, ignored if a binary or a hexadecimal flags are used */
		hexadecimal =  512,
		binary = 1024,
		json = 2048,
		space = 4096,
		pretty_print = 8192,
		numeric = 16384,
		date_only = 32768, /* use only year, month and day from pt::Date, no_time_zone flag is not used here */
		time_only = 65536, /* use only hour, min, sec from pt::Date, no_time_zone flag is not used here */
		no_time_zone = 131072, /* no time zone, used only with pt::Date */

		/*
		 * if this flag is set and a model does not have a primary key set
		 * then we print only 'null'
		 * (this is used only with flat strings)
		 *
		 */
		serialize_to_null_if_null = 262144,
		do_not_serialize_if_null = 524288, /* null objects are completely skipped when serializing to a flat string (test only for Model objects, not lists/vectors with childs models */
	};

	/*
	 * type can be a superposition from FieldType values
	 */
	int type;



	FT()
	{
		type = 0;
	}

	FT(const FT & field_type)
	{
		type = field_type.type;
	}

	FT(FieldType type)
	{
		this->type = static_cast<int>(type);
	}

	FT(int type)
	{
		this->type = type;
	}

	FT & operator=(const FT & field_type)
	{
		type = field_type.type;
		return *this;
	}

	FT & operator=(FieldType type)
	{
		this->type = static_cast<int>(type);
		return *this;
	}

	FT & operator=(int type)
	{
		this->type = type;
		return *this;
	}

	bool is_flag_set(int flag_mask) const
	{
		return (type & flag_mask) != 0;
	}


	bool is_primary_key() const
	{
		return is_flag_set(primary_key);
	}


	bool is_foreign_key() const
	{
		return is_flag_set(foreign_key);
	}

	bool is_foreign_key_in_child() const
	{
		return is_flag_set(foreign_key_in_child);
	}

	bool is_insertable() const
	{
		return !is_flag_set(no_insertable);
	}

	bool is_updatable() const
	{
		return !is_flag_set(no_updatable);
	}

	bool is_fetchable() const
	{
		return !is_flag_set(no_fetchable);
	}

	bool is_removable() const
	{
		return !is_flag_set(no_removable);
	}

	bool is_raw_field_name() const
	{
		return is_flag_set(raw_field_name);
	}

	bool use_utf8() const
	{
		return !is_flag_set(dont_use_utf8);
	}

	bool is_hexadecimal() const
	{
		return is_flag_set(hexadecimal);
	}

	bool is_binary() const
	{
		return is_flag_set(binary);
	}

	bool is_json() const
	{
		return is_flag_set(json);
	}

	bool is_space() const
	{
		return is_flag_set(space);
	}

	bool is_pretty_print() const
	{
		return is_flag_set(pretty_print);
	}

	bool is_numeric() const
	{
		return is_flag_set(numeric);
	}

	bool is_date_only() const
	{
		return is_flag_set(date_only);
	}

	bool is_time_only() const
	{
		return is_flag_set(time_only);
	}

	bool is_no_time_zone() const
	{
		return is_flag_set(no_time_zone);
	}

	bool is_serialize_to_null_if_null() const
	{
		return is_flag_set(serialize_to_null_if_null);
	}

	bool is_do_not_serialize_if_null() const
	{
		return is_flag_set(do_not_serialize_if_null);
	}

};

}

#endif

