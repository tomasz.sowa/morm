/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "jsonexpression.h"
#include "morm_types.h"
#include "convert/misc.h"


namespace morm
{


void JSONExpression::before_generate_from_model()
{
	BaseExpression::before_generate_from_model();

	if( work_mode == MORM_WORK_MODE_MODEL_FIELDS_VALUES )
	{
		(*out_stream) << "{";
	}
}


void JSONExpression::after_generate_from_model()
{
	BaseExpression::after_generate_from_model();

	if( work_mode == MORM_WORK_MODE_MODEL_FIELDS_VALUES )
	{
		(*out_stream) << "}";
	}
}



void JSONExpression::field_before()
{
	BaseExpression::field_before();

	if( !is_first_field )
	{
		(*out_stream) << ",";
	}
}



void JSONExpression::before_field_name()
{
	(*out_stream) << "\"";
}

void JSONExpression::after_field_name()
{
	(*out_stream) << "\"";
}



void JSONExpression::before_field_value_string(const FT & field_type, ModelEnv * model_env)
{
	(*out_stream) << "\"";
}

void JSONExpression::after_field_value_string(const FT & field_type, ModelEnv * model_env)
{
	(*out_stream) << "\"";
}


void JSONExpression::before_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_space() )
	{
		before_field_value_string(field_type, model_env);
	}
}

void JSONExpression::after_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_space() )
	{
		after_field_value_string(field_type, model_env);
	}
}



void JSONExpression::put_name_value_separator()
{
	(*out_stream) << ':';
}


void JSONExpression::before_field_value_list()
{
	(*out_stream) << "[";
}


void JSONExpression::after_field_value_list()
{
	(*out_stream) << "]";
}


bool JSONExpression::esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	return pt::try_esc_to_json(val, stream);
}



void JSONExpression::esc(const pt::Space & space, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	bool pretty_print = field_type.is_pretty_print();

	if( field_type.is_space() )
	{
		pt::TextStream tmp_stream;
		space.serialize_to_space_stream(tmp_stream, pretty_print);
		BaseExpression::esc(tmp_stream, stream, field_type, model_env);
	}
	else
	{
		// when serializing as json put it directly without escaping
		pt::TextStream tmp_stream;
		space.serialize_to_json_stream(tmp_stream, pretty_print);
		stream << tmp_stream;
	}
}

}
