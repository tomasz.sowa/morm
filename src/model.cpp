/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "model.h"
#include "convert/text.h"
#include "utf8/utf8.h"


namespace morm
{

Model::Model()
{
	model_connector = nullptr;
	model_env = nullptr;
	save_mode = DO_INSERT_ON_SAVE;
	has_primary_key_set = false;
}


Model::Model(const Model & m)
{
	model_connector = m.model_connector;
	save_mode = m.save_mode;
	model_env = nullptr;
	has_primary_key_set = m.has_primary_key_set;
}


Model::~Model()
{
}


void Model::set_save_mode(SaveMode save_mode)
{
	this->save_mode = save_mode;
}


Model::SaveMode Model::get_save_mode()
{
	return save_mode;
}


void Model::set_save_mode2(SaveMode save_mode, bool update_whole_tree)
{
	this->save_mode = save_mode;

	if( update_whole_tree )
	{
		ModelEnv model_env_local;
		model_env = &model_env_local;
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_PROPAGATE_SAVE_STATUS;
		fields();
		model_env = nullptr;
	}
}


void Model::set_has_primary_key_set(bool has_primary_key)
{
	this->has_primary_key_set = has_primary_key;
}


bool Model::get_has_primary_key_set() const
{
	return this->has_primary_key_set;
}



void Model::mark_to_delete()
{
	save_mode = DO_DELETE_ON_SAVE;
}

void Model::mark_to_remove()
{
	save_mode = DO_DELETE_ON_SAVE;
}

void Model::mark_to_insert()
{
	save_mode = DO_INSERT_ON_SAVE;
}


void Model::mark_to_update()
{
	save_mode = DO_UPDATE_ON_SAVE;
}



void Model::set_connector(ModelConnector & connector)
{
	set_connector(&connector);
}


void Model::set_connector(ModelConnector * connector)
{
	model_connector = connector;
}


ModelConnector * Model::get_connector()
{
	return model_connector;
}


void Model::table()
{
	if( model_connector )
	{
		pt::Log * plog = model_connector->get_logger();

		if( plog )
		{
			(*plog) << pt::Log::log1 << "Morm: you should provide the table name e.g. provide table() method and call table(...) there" << pt::Log::logend;
		}
	}
}

void Model::table_name(const wchar_t * table_name)
{
	if( model_env )
	{
		model_env->schema_name.clear();
		model_env->table_name.clear();

		model_env->table_name << table_name;
	}
}

void Model::table_name(const wchar_t * schema_name, const wchar_t * table_name)
{
	if( model_env )
	{
		model_env->schema_name.clear();
		model_env->table_name.clear();

		model_env->schema_name << schema_name;
		model_env->table_name << table_name;
	}
}



bool Model::object_exists()
{
	return save_mode == DO_UPDATE_ON_SAVE;
}


bool Model::found()
{
	return save_mode == DO_UPDATE_ON_SAVE;
}


void Model::get_table_name(pt::Stream & stream, bool with_schema_name, ModelData * model_data, bool clear_stream)
{
	if( clear_stream )
	{
		stream.clear();
	}

	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_data = model_data;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			try
			{
				table();

				if( with_schema_name && !model_env->schema_name.empty() )
				{
					stream << model_env->schema_name;
					stream << '.';
				}

				stream << model_env->table_name;
			}
			catch(...)
			{
				model_env = nullptr;
				throw;
			}
		}
	}

	model_env = nullptr;
}


void Model::get_table_name(std::wstring & str, bool with_schema_name, ModelData * model_data, bool clear_string)
{
	pt::WTextStream stream;

	if( clear_string )
		str.clear();

	get_table_name(stream, with_schema_name, model_data, false);
	stream.to_str(str);
}


void Model::get_table_name(std::string & str, bool with_schema_name, ModelData * model_data, bool clear_string)
{
	pt::WTextStream stream;

	get_table_name(stream, with_schema_name, model_data, false);
	pt::wide_stream_to_utf8(stream, str, clear_string);
}


Model * Model::get_model(const wchar_t * db_field_name, const wchar_t * flat_field_name)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GET_FIELD_MODEL;
	model_env->db_field_name = db_field_name;
	model_env->flat_field_name = flat_field_name;
	model_env->model = this;
	Model * model = nullptr;

	try
	{
		fields();
		model = model_env->child_model;
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model;
}


Wrapper Model::get_wrapper(const wchar_t * db_field_name, const wchar_t * flat_field_name)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GET_MODEL_WRAPPER;
	model_env->db_field_name = db_field_name;
	model_env->flat_field_name = flat_field_name;
	model_env->model = this;

	try
	{
		fields();
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.wrapper;
}



bool Model::get_raw_value(const wchar_t * db_field_name, const wchar_t * flat_field_name, pt::Stream & stream, bool clear_stream)
{
	return get_raw_value(db_field_name, flat_field_name, nullptr, stream, clear_stream);
}


bool Model::get_raw_value(const wchar_t * db_field_name, const wchar_t * flat_field_name, ModelData * model_data, pt::Stream & stream, bool clear_stream)
{
	if( clear_stream )
	{
		stream.clear();
	}

	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->has_primary_key_set = has_primary_key_set;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_PUT_FIELD_RAW_VALUE_TO_STREAM;
	model_env->db_field_name = db_field_name;
	model_env->flat_field_name = flat_field_name;
	model_env->model_data = model_data;
	model_env->stream = &stream;
	model_env->model = this;

	try
	{
		fields();
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.was_field_found;
}





void Model::to_text(pt::Stream & stream, ModelData * model_data, Export exp)
{
	if( !exp.is_no_clear_stream() )
	{
		stream.clear();
	}

	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->has_primary_key_set = has_primary_key_set;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_FLAT_STRING;
	model_env->dump_mode = exp.is_dump_mode();
	model_env->model_data = model_data;
	model_env->model = this;

	if( model_connector )
	{
		FlatConnector * flat_connector = model_connector->get_flat_connector();

		if( flat_connector )
		{
			try
			{
				// table(); at the moment flat strings (json/space) do not need a table name
				flat_connector->to_text(stream, *this, exp);
			}
			catch(...)
			{
				model_env = nullptr;
				throw;
			}
		}
	}

	model_env = nullptr;
}


void Model::to_text(pt::Stream & stream, ModelData & model_data, Export exp)
{
	to_text(stream, &model_data, exp);
}


void Model::to_text(pt::Stream & stream, Export exp)
{
	to_text(stream, nullptr, exp);
}


void Model::to_text(pt::Stream & stream, ModelData * model_data, bool clear_stream, bool dump_mode)
{
	Export exp = Export::default_type;

	if( !clear_stream )
		exp = exp.type | Export::no_clear_stream;

	if( dump_mode )
		exp = exp.type | Export::dump_mode;

	to_text(stream, model_data, exp);
}


void Model::to_text(pt::Stream & stream, ModelData & model_data, bool clear_stream, bool dump_mode)
{
	to_text(stream, &model_data, clear_stream, dump_mode);
}


void Model::to_text(pt::Stream & stream, bool clear_stream, bool dump_mode)
{
	to_text(stream, nullptr, clear_stream, dump_mode);
}




void Model::to_text(std::string & str, ModelData * model_data, bool clear_string, bool dump_mode)
{
	if( model_connector )
	{
		// CHECK ME what if the stream is being used by someone else?
		pt::Stream * out_stream = model_connector->get_stream();

		if( out_stream )
		{
			to_text(*out_stream, model_data, true, dump_mode);
			out_stream->to_str(str, clear_string);
		}
	}
}


void Model::to_text(std::string & str, ModelData & model_data, bool clear_string, bool dump_mode)
{
	to_text(str, &model_data, clear_string, dump_mode);
}


void Model::to_text(std::string & str, bool clear_string, bool dump_mode)
{
	to_text(str, nullptr, clear_string, dump_mode);
}


std::string Model::to_text()
{
	std::string str;

	to_text(str, false);
	return str;
}


std::string Model::to_string()
{
	return to_text();
}




void Model::generate_insert_query(pt::Stream & stream, ModelData * model_data)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->has_primary_key_set = has_primary_key_set;
	model_env->model_data = model_data;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
	model_env->model = this;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			try
			{
				table();
				db_connector->generate_insert_query(stream, *this);
			}
			catch(...)
			{
				model_env = nullptr;
				throw;
			}
		}
	}

	model_env = nullptr;
}


bool Model::insert(ModelData & model_data, bool insert_whole_tree)
{
	return insert(&model_data, insert_whole_tree);
}


bool Model::insert(bool insert_whole_tree)
{
	return insert(nullptr, insert_whole_tree);
}


bool Model::insert(ModelData * model_data, bool insert_whole_tree)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_data = model_data;
	model_env->model = this;

	try
	{
		table();
		insert_tree(insert_whole_tree);
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.status;
}


void Model::insert_tree(bool insert_whole_tree)
{
	model_env->has_primary_key_set = has_primary_key_set;

	if( model_env->status && insert_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_INSERT;
		fields();
	}

	if( model_env->status && model_connector )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
		DbConnector * db_connector = model_connector->get_db_connector();

		// CHECK ME what if the stream is being used by someone else?
		pt::Stream * out_stream = model_connector->get_stream();

		if( db_connector && out_stream )
		{

			before_insert();
			out_stream->clear();
			model_env->status = db_connector->insert(*out_stream, *this);

			if( model_env->status )
			{
				/*
				 * after_insert() should read the new primary key and set has_primary_key_set flag if the key was read correctly
				 */
				after_insert();
				model_env->has_primary_key_set = has_primary_key_set;

				if( has_primary_key_set )
				{
					save_mode = DO_UPDATE_ON_SAVE;
					set_parent_key_in_childs(); // may it would be better to set it even if we do not have a primary key? set it to zero or something?
				}
				else
				{
					save_mode = DO_NOTHING_ON_SAVE;
				}
			}
			else
			{
				after_insert_failure();
			}
		}
		else
		{
			model_env->status = false;
		}
	}
	else
	{
		model_env->status = false;
	}

	if( model_env->status && insert_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_INSERT;
		fields();
	}
}



void Model::generate_update_query(pt::Stream & stream, ModelData * model_data)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->has_primary_key_set = has_primary_key_set;
	model_env->model_data = model_data;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
	model_env->model = this;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			table();
			db_connector->generate_update_query(stream, *this);
		}
	}

	// what about if an exception was thrown? this pointer will not be null
	model_env = nullptr;
}


bool Model::update(ModelData & model_data, bool update_whole_tree)
{
	return update(&model_data, update_whole_tree);
}


bool Model::update(bool update_whole_tree)
{
	return update(nullptr, update_whole_tree);
}


bool Model::update(ModelData * model_data, bool update_whole_tree)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_data = model_data;
	model_env->model = this;

	try
	{
		table();
		update_tree(update_whole_tree);
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.status;
}


void Model::update_tree(bool update_whole_tree)
{
	model_env->has_primary_key_set = has_primary_key_set;

	if( !has_primary_key_set )
	{
		put_to_log(L"Morm: call update but model doesn't have a primary key set");
		model_env->status = false;
	}

	if( model_env->status && update_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_UPDATE;
		fields();
	}

	if( model_env->status && model_connector )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
		DbConnector * db_connector = model_connector->get_db_connector();

		// CHECK ME what if the stream is being used by something other?
		pt::Stream * out_stream = model_connector->get_stream();

		if( db_connector && out_stream )
		{
			before_update();
			out_stream->clear();
			model_env->status = db_connector->update(*out_stream, *this);

			if( model_env->status )
				after_update();
			else
				after_update_failure();
		}
		else
		{
			model_env->status = false;
		}
	}
	else
	{
		model_env->status = false;
	}

	if( model_env->status && update_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_UPDATE;
		fields();
	}
}


void Model::generate_remove_query(pt::Stream & stream, ModelData * model_data)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->has_primary_key_set = has_primary_key_set;
	model_env->model_data = model_data;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
	model_env->model = this;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			table();
			db_connector->generate_remove_query(stream, *this);
		}
	}

	// what about if an exception was thrown? this pointer will not be null
	model_env = nullptr;
}



bool Model::remove(ModelData & model_data, bool remove_whole_tree)
{
	return remove(&model_data, remove_whole_tree);
}


bool Model::remove(bool remove_whole_tree)
{
	return remove(nullptr, remove_whole_tree);
}


bool Model::remove(ModelData * model_data, bool remove_whole_tree)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_data = model_data;
	model_env->model = this;

	try
	{
		table();
		remove_tree(remove_whole_tree);
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.status;
}


void Model::remove_tree(bool remove_whole_tree)
{
	model_env->has_primary_key_set = has_primary_key_set;

	if( !has_primary_key_set )
	{
		put_to_log(L"Morm: call remove but model doesn't have a primary key set");
		model_env->status = false;
	}

	if( model_env->status && remove_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_REMOVE;
		fields();
	}

	if( model_env->status && model_connector )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
		DbConnector * db_connector = model_connector->get_db_connector();

		// CHECK ME what if the stream is being used by someone else?
		pt::Stream * out_stream = model_connector->get_stream();

		if( db_connector && out_stream )
		{
			before_remove();
			out_stream->clear();
			model_env->status = db_connector->remove(*out_stream, *this);

			if( model_env->status )
			{
				save_mode = DO_NOTHING_ON_SAVE;
				has_primary_key_set = false;
				model_env->has_primary_key_set = false;
				after_remove();
			}
			else
			{
				after_remove_failure();
			}
		}
		else
		{
			model_env->status = false;
		}
	}
	else
	{
		model_env->status = false;
	}

	if( model_env->status && remove_whole_tree )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY;
		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_REMOVE;
		fields();
	}
}




bool Model::save(ModelData & model_data, bool save_whole_tree)
{
	return save(&model_data, save_whole_tree);
}


bool Model::save(bool save_whole_tree)
{
	return save(nullptr, save_whole_tree);
}


bool Model::save(ModelData * model_data, bool save_whole_tree)
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_data = model_data;
	model_env->model = this;

	try
	{
		table();
		save_tree(save_whole_tree);
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	return model_env_local.status;
}


void Model::save_tree(bool save_whole_tree)
{
	model_env->has_primary_key_set = has_primary_key_set;
	SaveMode origin_save_mode = save_mode;

	if( model_env->status && save_whole_tree )
	{
		if( save_mode == DO_DELETE_ON_SAVE )
			model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY;
		else
			model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY;

		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_SAVE;
		fields();
	}

	if( model_env->status )
	{
		ModelEnv * old_model_env = model_env; // remove, insert or update will set model_env to nullptr

		switch( save_mode )
		{
		case DO_DELETE_ON_SAVE:
			remove_tree(false);
			break;

		case DO_INSERT_ON_SAVE:
			insert_tree(false);
			break;

		case DO_UPDATE_ON_SAVE:
			update_tree(false);
			break;

		case DO_NOTHING_ON_SAVE:
			break;
		}

		model_env = old_model_env;
	}

	if( model_env->status && save_whole_tree )
	{
		if( origin_save_mode == DO_DELETE_ON_SAVE )
			model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY;
		else
			model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY;

		model_env->model_work_submode = MORM_MODEL_WORK_SUBMODE_SAVE;
		fields();
	}
}


void Model::generate_select_columns(pt::Stream & stream)
{
	if( model_connector && model_env )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			db_connector->generate_select_columns(stream, *this);
		}
	}
}





void Model::map_values_from_query()
{
	if( model_env )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_READING_VALUE_FROM_DB_RESULTSET;
		model_env->was_primary_key_read = false; // whether or not there was at least one column with primary_key flag
		model_env->has_primary_key_set = true;   // whether all primary_columns were different than null
		fields();
		map_additional_columns_from_query();
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_NONE;

		if( model_env->was_primary_key_read && model_env->has_primary_key_set )
		{
			has_primary_key_set = true;
			save_mode = DO_UPDATE_ON_SAVE;
		}
		else
		{
			has_primary_key_set = false;
			save_mode = DO_NOTHING_ON_SAVE;
		}
	}
}


void Model::map_additional_columns_from_query()
{
	if( model_env )
	{
		if( model_env->select_flags.is_with_rows_counter() )
		{
			map_rows_counter_from_query();
		}
	}
}


void Model::map_rows_counter_from_query()
{
	if( model_env && model_env->cursor_helper )
	{
		/*
		 * take the value only from the first row, the value should be the same on every row
		 */
		if( model_env->cursor_helper->current_row == 0 )
		{
			field(model_env->rows_counter_column_name.c_str(), L"", model_env->rows_counter);
		}
		else
		{
			if( model_env->cursor_helper && model_env->cursor_helper->has_autogenerated_select )
			{
				model_env->cursor_helper->current_column += 1;
			}
		}
	}
}



void Model::clear()
{
	ModelEnv model_env_local;
	model_env = &model_env_local;
	model_env->model_work_mode = MORM_MODEL_WORK_MODE_CLEARING_VALUE;
	model_env->model = this;

	try
	{
		// table() doesn't have to be called
		fields();
	}
	catch(...)
	{
		model_env = nullptr;
		throw;
	}

	model_env = nullptr;
	save_mode = DO_INSERT_ON_SAVE;
	has_primary_key_set = false;
}



bool Model::do_migration(int & current_table_version)
{
	return true;
}




void Model::before_select()
{
}

void Model::before_insert()
{
}

void Model::before_update()
{
}

void Model::before_remove()
{
}

void Model::after_select()
{
}

void Model::after_insert()
{
}

void Model::after_update()
{
}

void Model::after_remove()
{
}

void Model::after_select_failure()
{
}

void Model::after_insert_failure()
{
}

void Model::after_update_failure()
{
}

void Model::after_remove_failure()
{
}

int Model::get_work_mode()
{
	if( model_env )
	{
		return model_env->model_work_mode;
	}
	else
	{
		return MORM_MODEL_WORK_MODE_NONE;
	}
}


ModelData * Model::get_model_data()
{
	if( model_env )
	{
		return model_env->model_data;
	}
	else
	{
		return nullptr;
	}
}


bool Model::is_empty_field(const wchar_t * value)
{
	return (!value || *value == '\0');
}


bool Model::is_the_same_field(const wchar_t * field1, const wchar_t * field2)
{
	if( is_empty_field(field1) && is_empty_field(field2) )
		return true;

	if( is_empty_field(field1) || is_empty_field(field2) )
		return false;

	return pt::is_equal(field1, field2);
}



void Model::log_table_name(bool put_schema_name)
{
	if( model_connector && model_env )
	{
		pt::Log * plog = model_connector->get_logger();

		if( plog )
		{
			if( put_schema_name && !model_env->schema_name.empty() )
			{
				(*plog) << model_env->schema_name;

				// although in BaseExpression there is schema_table_separator() method
				// but for logging purposes we can use just a dot here
				(*plog) << '.';
			}

			(*plog) << model_env->table_name;
		}
	}
}


void Model::log_table_name_with_field(const wchar_t * db_field_name, bool put_schema_name)
{
	if( model_connector && model_env )
	{
		pt::Log * plog = model_connector->get_logger();

		if( plog )
		{
			bool is_empty_field_name = is_empty_field(db_field_name);

			if( put_schema_name && !model_env->schema_name.empty() )
			{
				(*plog) << model_env->schema_name;
				(*plog) << '.';
			}

			(*plog) << model_env->table_name;

			if( !is_empty_field_name )
			{
				(*plog) << '.';
				(*plog) << db_field_name;
			}
		}
	}
}


pt::Log * Model::get_logger()
{
	if( model_connector )
	{
		return model_connector->get_logger();
	}

	return nullptr;
}


void Model::put_to_log(const wchar_t * str)
{
	pt::Log * log = get_logger();

	if( log )
	{
		(*log) << str << pt::Log::logend;
	}
}


void Model::put_fields_to_log(pt::Log & log, const wchar_t * db_field_name, const wchar_t * flat_field_name)
{
	bool was_db_field_put = false;
	bool was_flat_field_put = false;

	if( !is_empty_field(db_field_name) )
	{
		log << "database field name: " << db_field_name;
		was_db_field_put = true;
	}

	if( !is_empty_field(flat_field_name) )
	{
		if( was_db_field_put )
			log << ", ";

		log << "flat field name: " << flat_field_name;
		was_flat_field_put = true;
	}

	if( !was_db_field_put && !was_flat_field_put )
	{
		log << "(both database field name and flat field name are empty)";
	}
}


/*
 * IMPROVE ME there can be more rows in the result set when there are more items on the left hand side of the join
 * this is only in a case when has_foreign_key is false, may it can be ignored? we can use unique index in such a case
 *
 */
void Model::field_model_left_join(const wchar_t * db_field_name, Model & field_model, const FT & field_type, DbExpression * db_expression)
{
	if( model_env && field_model.model_env && model_env->finder_helper )
	{
		model_env->finder_helper->foreign_keys.clear();
		pt::TextStream & join_tables_str = model_env->finder_helper->join_tables_str;
		field_model.model_env->add_table_name_to_finder_helper();

		join_tables_str << "LEFT JOIN ";

		pt::Stream * db_expression_stream = db_expression->get_text_stream();
		int expr_work_mode = db_expression->get_work_mode();
		int expr_output_type = db_expression->get_output_type();
		bool expr_allow_prefix = db_expression->get_allow_to_use_prefix();

		db_expression->schema_table_to_stream(join_tables_str, field_model.model_env->schema_name, field_model.model_env->table_name);
		join_tables_str << " AS ";
		db_expression->alias_to_stream(join_tables_str, field_model.model_env->table_name, field_model.model_env->table_index);

		db_expression->set_work_mode(MORM_WORK_MODE_MODEL_SAVE_FIELDS);
		db_expression->set_output_type(MORM_OUTPUT_TYPE_JOIN_TABLES);
		db_expression->allow_to_use_prefix(false);
		db_expression->set_text_stream(db_expression_stream); // restore original value because schema_table_to_stream() will set the stream to null

		if( field_type.is_foreign_key() )
		{
			field_model.fields();

			join_tables_str << " ON ";
			db_expression->table_with_index_and_field_to_stream(join_tables_str, model_env->table_name, model_env->table_index, db_field_name, field_type);
			join_tables_str << " = ";

			db_expression->table_with_index_to_stream(join_tables_str, field_model.model_env->table_name, field_model.model_env->table_index);
			join_tables_str << '.';

			// IMPROVE ME at the moment support only for foreign keys consisting of only one column
			if( model_env->finder_helper->foreign_keys.size() == 1 )
			{
				join_tables_str << model_env->finder_helper->foreign_keys.front();
			}
		}
		else
		{
			ModelEnv * old_model_env = field_model.model_env;
			fields(); // fields() will set field_model.model_env to null
			field_model.model_env =  old_model_env;

			join_tables_str << " ON ";
			db_expression->table_with_index_to_stream(join_tables_str, model_env->table_name, model_env->table_index);
			join_tables_str << '.';

			// IMPROVE ME at the moment support only for foreign keys consisting of only one column
			if( model_env->finder_helper->foreign_keys.size() == 1 )
			{
				join_tables_str << model_env->finder_helper->foreign_keys.front();
			}

			join_tables_str << " = ";
			db_expression->table_with_index_and_field_to_stream(join_tables_str, field_model.model_env->table_name, field_model.model_env->table_index, db_field_name, field_type);
		}

		join_tables_str << ' ';

		db_expression->set_work_mode(expr_work_mode);
		db_expression->set_output_type(expr_output_type);
		db_expression->allow_to_use_prefix(expr_allow_prefix);
		db_expression->set_text_stream(db_expression_stream);
	}
}


/*
 * first we iterate through fields and save primary key values to helper_tab
 */
void Model::field_model_save_key(const wchar_t * db_field_name)
{
	DbConnector * db_connector = model_connector->get_db_connector();
	pt::Log * plog = model_connector->get_logger();

	if( db_connector )
	{
		DbExpression * db_expression = db_connector->get_expression();

		if( db_expression && !is_empty_field(db_field_name) && model_env->field_value_helper_tab )
		{
			int old_work_mode = model_env->model_work_mode;
			model_env->model_work_mode = MORM_MODEL_WORK_MODE_ITERATE_PRIMARY_KEY_VALUES;
			model_env->field_index = 0;
			fields();
			model_env->model_work_mode = old_work_mode;

			if( model_env->field_value_helper_tab->empty() && plog )
			{
				(*plog) << pt::Log::log1 << "Morm: I cannot find a primary key in ";
				log_table_name();
				(*plog) << pt::Log::logend;
			}
		}
	}
}


/*
 * now we iterate through fields in field_model and save primary key values from *this object to the specified fields in field_model
 */
void Model::field_model_set_parent_key_in_child(const wchar_t * db_field_name, Model & field_model)
{
	DbConnector * db_connector = model_connector->get_db_connector();
	pt::Log * log = model_connector->get_logger();

	if( db_connector )
	{
		DbExpression * db_expression = db_connector->get_expression();

		if( db_expression && !is_empty_field(db_field_name) && model_env->field_value_helper_tab )
		{
			std::vector<FieldValueHelper> & helper_tab = *model_env->field_value_helper_tab;

			if( (size_t)model_env->field_index == helper_tab.size() )
			{
				ModelEnv model_env_local;
				model_env_local.copy_global_objects(*model_env);
				model_env_local.has_primary_key_set = field_model.has_primary_key_set;
				model_env_local.model_work_mode = MORM_MODEL_WORK_MODE_SET_FIELD_VALUE;
				model_env_local.field_value_helper_tab = &helper_tab;
				model_env_local.field_index = 0;
				model_env_local.model = &field_model;

				field_model.model_env = &model_env_local;
				field_model.table();
				field_model.fields();

				if( (size_t)field_model.model_env->field_index != helper_tab.size() && log )
				{
					if( field_model.model_env->field_index == 0 )
					{
						(*log) << pt::Log::log1 << "Morm: there is no a foreign key in ";
						field_model.log_table_name();
						(*log) << " called " << db_field_name << " pointing to ";
						log_table_name();
						(*log) << pt::Log::logend;
					}
					else
					{
						(*log) << pt::Log::log1 << "Morm: primary key in ";
						log_table_name();
						(*log) << " consists of " << model_env->field_index << " column(s) but foreign key in ";
						field_model.log_table_name();
						(*log) << " consists of " << field_model.model_env->field_index << " column(s)" << pt::Log::logend;
					}
				}

				field_model.model_env = nullptr;
			}
			else
			if( log )
			{
				(*log) << pt::Log::log1 << "Morm: primary key in ";
				log_table_name();
				(*log) << " consists of incorrect number of columns, expected " << helper_tab.size()
					   << " column(s) but got " << model_env->field_index << pt::Log::logend;
			}
		}
	}
}


void Model::field_model_set_parent_key(const wchar_t * db_field_name, Model & field_model)
{
	FieldValueHelper helper;
	helper.db_field_name = db_field_name;
	helper.flat_field_name = nullptr;
	helper.compare_flat_field_name = false;

	std::vector<FieldValueHelper> helper_tab;
	helper_tab.push_back(helper);
	// only one column at the moment, in the future we can have a primary key from more than one column

	model_env->field_value_helper_tab = &helper_tab;

	field_model_save_key(db_field_name);
	field_model_set_parent_key_in_child(db_field_name, field_model);

	model_env->field_value_helper_tab = nullptr;
}


void Model::field_model_iterate_through_childs(Model & field_model, const FT & field_type)
{
	if( model_env->model_work_submode == MORM_MODEL_WORK_SUBMODE_INSERT )
	{
		if( field_type.is_insertable() )
			field_model.insert_tree(true);
	}

	if( model_env->model_work_submode == MORM_MODEL_WORK_SUBMODE_UPDATE )
	{
		if( field_type.is_updatable() )
			field_model.update_tree(true);
	}

	if( model_env->model_work_submode == MORM_MODEL_WORK_SUBMODE_REMOVE )
	{
		if( field_type.is_removable() )
			field_model.remove_tree(true);
	}

	if( model_env->model_work_submode == MORM_MODEL_WORK_SUBMODE_SAVE )
	{
		if( (field_model.save_mode == Model::DO_INSERT_ON_SAVE && field_type.is_insertable()) ||
			(field_model.save_mode == Model::DO_UPDATE_ON_SAVE && field_type.is_updatable()) ||
			(field_model.save_mode == Model::DO_DELETE_ON_SAVE && field_type.is_removable()) )
		{
			field_model.save_tree(true);
		}
	}
}



void Model::field_model_generate_flat_string(const wchar_t * flat_field_name, Model & field_model, const FT & field_type)
{
	FlatConnector * flat_connector = model_connector->get_flat_connector();

	if( flat_connector )
	{
		FlatExpression * flat_expression = flat_connector->get_expression();

		if( flat_expression )
		{
			if( model_env->dump_mode || field_model.save_mode != DO_DELETE_ON_SAVE )
			{
				field_model.model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_FLAT_STRING;
				flat_expression->field_model(flat_field_name, field_model, field_type, model_env);
			}
		}
	}
}


void Model::field_model_generate_db_sql(const wchar_t * db_field_name, Model & field_model, const FT & field_type)
{
	DbConnector * db_connector = model_connector->get_db_connector();

	if( db_connector )
	{
		DbExpression * db_expression = db_connector->get_expression();

		if( db_expression && !is_empty_field(db_field_name) )
		{
			field_model.model_env->model_work_mode = MORM_MODEL_WORK_MODE_GENERATING_DB_SQL;

			if( db_expression->get_output_type() == MORM_OUTPUT_TYPE_SELECT_COLUMNS )
			{
				field_model_left_join(db_field_name, field_model, field_type, db_expression);
			}

			if( field_type.is_foreign_key() )
			{
				if( db_expression->get_work_mode() == MORM_WORK_MODE_MODEL_FIELDS && db_expression->get_output_type() == MORM_OUTPUT_TYPE_DB_INSERT )
				{
					if( field_type.is_insertable() )
					{
						int not_used_object = 0;
						db_expression->field(db_field_name, not_used_object, field_type, model_env);
					}
				}

				if( db_expression->get_work_mode() == MORM_WORK_MODE_MODEL_VALUES && db_expression->get_output_type() == MORM_OUTPUT_TYPE_DB_INSERT )
				{
					if( field_type.is_insertable() )
					{
						db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_INSERT_PRIMARY_KEY);
						field_model.fields();
						db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_INSERT);
					}
				}

				if( db_expression->get_work_mode() == MORM_WORK_MODE_MODEL_FIELDS_VALUES && db_expression->get_output_type() == MORM_OUTPUT_TYPE_DB_UPDATE )
				{
					if( field_type.is_updatable() )
					{
						std::vector<const wchar_t *> key_fields;
						key_fields.push_back(db_field_name); // at the moment only one key

						db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_UPDATE_PRIMARY_KEY);
						field_model.model_env->field_index = 0;
						field_model.model_env->set_field_name_helper = &key_fields;
						field_model.fields();
						db_expression->set_output_type(MORM_OUTPUT_TYPE_DB_UPDATE);

						if( (size_t)field_model.model_env->field_index != key_fields.size() )
						{
							// IMPROVEME
							// number of keys are different
							// put error log here
						}
					}
				}
			}

			if( db_expression->get_output_type() != MORM_OUTPUT_TYPE_JOIN_TABLES &&
				db_expression->get_output_type() != MORM_OUTPUT_TYPE_DB_PRIMARY_KEY &&
				db_expression->get_output_type() != MORM_OUTPUT_TYPE_DB_INSERT &&
				db_expression->get_output_type() != MORM_OUTPUT_TYPE_DB_UPDATE &&
				db_expression->get_output_type() != MORM_OUTPUT_TYPE_DB_INSERT_PRIMARY_KEY &&
				db_expression->get_output_type() != MORM_OUTPUT_TYPE_DB_UPDATE_PRIMARY_KEY )
			{
				field_model.fields();
			}

			field_model.model_env->model_work_mode = MORM_MODEL_WORK_MODE_NONE;
		}
	}
}


void Model::field_model_clear_values(Model & field_model, const FT & field_type)
{
	Clearer * clearer = model_connector->get_clearer();

	if( clearer )
	{
		clearer->clear_model(field_model, field_type);
	}
}



void Model::field_model_read_values_from_queryresult(const wchar_t * db_field_name, Model & field_model, const FT & field_type)
{
	DbConnector * db_connector = model_connector->get_db_connector();

	if( db_connector )
	{
		DbExpression * db_expression = db_connector->get_expression();

		if( db_expression )
		{
			if( model_env->cursor_helper &&
				!model_env->cursor_helper->has_autogenerated_select &&
				model_env->cursor_helper->use_table_prefix_for_fetching_values )
			{
				field_model.model_env->add_table_name_to_finder_helper();
			}

			field_model.before_select();
			field_model.map_values_from_query();

			if( field_model.found() )
			{
				field_model.after_select();
			}
		}
	}
}


void Model::field_model(const wchar_t * db_field_name, const wchar_t * flat_field_name, Model & field_model, const FT & field_type)
{
	if( model_connector && model_env )
	{
		ModelEnv model_env_local;
		model_env_local.copy_global_objects(*model_env);
		model_env_local.model = &field_model;

		field_model.model_env = &model_env_local;
		field_model.model_env->has_primary_key_set = field_model.has_primary_key_set;
		field_model.set_connector(model_connector);

		if( !is_empty_field(db_field_name) )
		{
			field_model.table();

			if( field_type.is_foreign_key() || field_type.is_foreign_key_in_child() )
			{
				field_model_for_db(db_field_name, field_model, field_type);
			}
			else
			{
				pt::Log * plog = model_connector->get_logger();

				if( plog )
				{
					(*plog) << pt::Log::log1 << "Morm: error in ";
					log_table_name_with_field(db_field_name);
					(*plog) << " field, you should set FT::is_foreign_key or FT::is_foreign_key_in_child flag for a model child object" << pt::Log::logend;
				}
			}
		}

		if( !is_empty_field(flat_field_name) )
		{
			if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GENERATING_FLAT_STRING )
			{
				// calling field_model.table() is not needed in generating strings (at least for json/space formats)
				field_model_generate_flat_string(flat_field_name, field_model, field_type);
			}
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_CLEARING_VALUE )
		{
			field_model_clear_values(field_model, field_type);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GET_FIELD_MODEL )
		{
			if( is_the_same_field(db_field_name, model_env->db_field_name) &&
				is_the_same_field(flat_field_name, model_env->flat_field_name) )
			{
				model_env->child_model = &field_model;
			}
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GET_MODEL_WRAPPER )
		{
			if( (is_empty_field(model_env->db_field_name) || is_the_same_field(db_field_name, model_env->db_field_name)) &&
				(is_empty_field(model_env->flat_field_name) || is_the_same_field(flat_field_name, model_env->flat_field_name)) &&
				!model_env->wrapper.model )
			{
				model_env->wrapper.model = &field_model;
			}
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_PROPAGATE_SAVE_STATUS )
		{
			field_model.set_save_mode2(save_mode, true);
		}

		field_model.model_env = nullptr;

		if( !model_env_local.status )
			model_env->status = false;
	}
}



void Model::field_model_for_db(const wchar_t * db_field_name, Model & field_model, const FT & field_type)
{
	if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_SET_PARENT_ID )
	{
		if( field_type.is_foreign_key_in_child() )
		{
			field_model_set_parent_key(db_field_name, field_model);
		}
	}

	if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITH_FOREIGN_KEY )
	{
		if( field_type.is_foreign_key() )
		{
			field_model_iterate_through_childs(field_model, field_type);
		}
	}

	if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_ITERATE_THROUGH_CHILDS_WITHOUT_FOREIGN_KEY )
	{
		if( field_type.is_foreign_key_in_child() )
		{
			field_model_iterate_through_childs(field_model, field_type);
		}
	}

	if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GENERATING_DB_SQL )
	{
		field_model_generate_db_sql(db_field_name, field_model, field_type);
	}


	if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_READING_VALUE_FROM_DB_RESULTSET )
	{
		field_model_read_values_from_queryresult(db_field_name, field_model, field_type);
	}
}


void Model::set_parent_key_in_childs()
{
	if( model_env )
	{
		model_env->model_work_mode = MORM_MODEL_WORK_MODE_SET_PARENT_ID;
		fields();
	}
}



bool Model::db_query(const char * raw_sql)
{
	bool status = false;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			status = db_connector->query(raw_sql);
		}
	}

	return status;
}


bool Model::db_query(const std::string & raw_sql)
{
	return db_query(raw_sql.c_str());
}


bool Model::db_query(const pt::Stream & raw_sql)
{
	bool status = false;

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			status = db_connector->query(raw_sql);
		}
	}

	return status;
}


bool Model::db_query(const char ** raw_sql, size_t len)
{
	for(size_t i=0 ; i < len ; ++i)
	{
		if( !db_query(raw_sql[i]) )
		{
			return false;
		}
	}

	return true;
}





void Model::field_member_set_field_value(
		const wchar_t * db_field_name,
		const wchar_t * flat_field_name,
		void (Model::*setter_method)(const char * input_str))
{
	if( model_env->field_value_helper_tab )
	{
		if( model_env->field_index >= 0 && (size_t)model_env->field_index < model_env->field_value_helper_tab->size() )
		{
			FieldValueHelper & helper = (*model_env->field_value_helper_tab)[model_env->field_index];
			pt::Log * log = model_connector->get_logger();

			if( (!helper.compare_db_field_name || is_the_same_field(db_field_name, helper.db_field_name)) &&
				(!helper.compare_flat_field_name || is_the_same_field(flat_field_name, helper.flat_field_name)) )
			{
				if( helper.value_object && helper.value_type_info )
				{
					const char * p_const_char_type;

					if( typeid(p_const_char_type) == *helper.value_type_info )
					{
						const char * const * p_str = reinterpret_cast<const char * const *>(helper.value_object);
						(model_env->model->*setter_method)(*p_str);
					}
					else
					{
						if( log )
						{
							(*log) << pt::Log::log1 << "Morm: incorrect type of a field in ";
							log_table_name();
							(*log) << ", ";
							put_fields_to_log(*log, db_field_name, flat_field_name);
							(*log) << ", type expected " << typeid(p_const_char_type).name()
								   << " got " << helper.value_type_info->name() << pt::Log::logend;
						}
					}
				}

				helper.found = true;
				model_env->field_index += 1;
			}
		}
	}
}


void Model::field_member_generate_flat_string(
		const wchar_t * flat_field_name,
		void (Model::*getter_method)(pt::Stream &),
		const FT & field_type)
{
	FlatConnector * flat_connector = model_connector->get_flat_connector();

	if( flat_connector )
	{
		FlatExpression * flat_expression = flat_connector->get_expression();

		if( flat_expression && !is_empty_field(flat_field_name) )
		{
			flat_expression->field(flat_field_name, getter_method, field_type, model_env);
		}
	}
}


void Model::field_member_generate_db_sql(
		const wchar_t * db_field_name,
		void (Model::*getter_method)(pt::Stream &),
		const FT & field_type)
{
	DbConnector * db_connector = model_connector->get_db_connector();

	if( db_connector )
	{
		DbExpression * db_expression = db_connector->get_expression();

		if( db_expression && !is_empty_field(db_field_name) )
		{
			// dodac do FT raw_value
			// jest raw_value jest zdefiniowane to nie wykonujemy eskejpowania
			// (jesli nie zdefiniowane to zrobic tymczasowy strumien ktory zostanie dostarczony
			// do getter_method i pozniej z tego strumienia wykopiowac z eskejpowaniem i wstawic do normalnego strumienia)

			db_expression->field(db_field_name, getter_method, field_type, model_env);
		}
	}
}


void Model::field_member_read_value_from_db_resultset(
		const wchar_t * db_field_name,
		void (Model::*setter_method)(const char * input_str),
		const FT & field_type)
{
	if( model_env && model_env->model )
	{
		if( field_type.is_binary() || field_type.is_hexadecimal() )
		{
			std::string db_value;
			field_generic_read_value_from_db_resultset(db_field_name, db_value, field_type);
			(model_env->model->*setter_method)(db_value.c_str());
		}
		else
		{
			std::string_view db_value;
			field_generic_read_value_from_db_resultset(db_field_name, db_value, field_type);

			if( db_value.data() != nullptr )
			{
				// the string is already null terminated (by the database)
				(model_env->model->*setter_method)(db_value.data());
			}
		}
	}
}


void Model::field_member_put_field_raw_value_to_stream(
		const wchar_t * db_field_name,
		const wchar_t * flat_field_name,
		void (Model::*getter_method)(pt::Stream &),
		const FT & field_type)
{
	if( (is_empty_field(model_env->db_field_name) || is_the_same_field(db_field_name, model_env->db_field_name)) &&
		(is_empty_field(model_env->flat_field_name) || is_the_same_field(flat_field_name, model_env->flat_field_name)) )
	{
		model_env->was_field_found = true;

		if( model_env->stream )
		{
			(model_env->model->*getter_method)(*model_env->stream);
		}
	}
}


void Model::field_member_clear_value(
		void (Model::*setter_method)(const char * input_str),
		const FT & field_type)
{
	char buffer[1] = {0};
	(model_env->model->*setter_method)(buffer);
}


void Model::field_member(
		const wchar_t * db_field_name,
		const wchar_t * flat_field_name,
		void (Model::*getter_method)(pt::Stream &),
		void (Model::*setter_method)(const char * input_str),
		const FT & field_type)
{
	if( model_connector && model_env && model_env->model )
	{
		if( field_type.is_primary_key() )
		{
			pt::Log * plog = model_connector->get_logger();

			if( plog )
			{
				(*plog) << pt::Log::log1 << "Morm:: getters and setters cannot be used as a primary key" << pt::Log::logend;
			}
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_SET_FIELD_VALUE )
		{
			field_member_set_field_value(db_field_name, flat_field_name, setter_method);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GENERATING_FLAT_STRING )
		{
			field_member_generate_flat_string(flat_field_name, getter_method, field_type);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_GENERATING_DB_SQL )
		{
			field_member_generate_db_sql(db_field_name, getter_method, field_type);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_READING_VALUE_FROM_DB_RESULTSET )
		{
			field_member_read_value_from_db_resultset(db_field_name, setter_method, field_type);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_PUT_FIELD_RAW_VALUE_TO_STREAM )
		{
			field_member_put_field_raw_value_to_stream(db_field_name, flat_field_name, getter_method, field_type);
		}

		if( model_env->model_work_mode == MORM_MODEL_WORK_MODE_CLEARING_VALUE )
		{
			field_member_clear_value(setter_method, field_type);
		}
	}
}



bool Model::convert_to_bool(char v)
{
	return v != 0;
}

bool Model::convert_to_bool(unsigned char v)
{
	return v != 0;
}

bool Model::convert_to_bool(wchar_t v)
{
	return v != 0;
}


bool Model::convert_to_bool(const std::wstring & str)
{
	return !str.empty();
}

bool Model::convert_to_bool(const wchar_t * str)
{
	return str != nullptr && *str != 0;
}


bool Model::convert_to_bool(const std::string & str)
{
	return !str.empty();
}

bool Model::convert_to_bool(const char * str)
{
	return str != nullptr && *str != 0;
}


bool Model::convert_to_bool(bool v)
{
	return v;
}


bool Model::convert_to_bool(short v)
{
	return v != 0;
}

bool Model::convert_to_bool(unsigned short v)
{
	return v != 0;
}

bool Model::convert_to_bool(int v)
{
	return v != 0;
}

bool Model::convert_to_bool(unsigned int v)
{
	return v != 0;
}

bool Model::convert_to_bool(long v)
{
	return v != 0;
}

bool Model::convert_to_bool(unsigned long v)
{
	return v != 0;
}

bool Model::convert_to_bool(long long v)
{
	return v != 0;
}

bool Model::convert_to_bool(unsigned long long v)
{
	return v != 0;
}


bool Model::convert_to_bool(float v)
{
	return v != 0.0F;
}

bool Model::convert_to_bool(double v)
{
	return v != 0.0;
}

bool Model::convert_to_bool(long double v)
{
	return v != 0.0L;
}


bool Model::convert_to_bool(const pt::Date & date)
{
	// may return true if the date is not the Unix epoch?
	return false;
}

bool Model::convert_to_bool(const pt::Stream & val)
{
	return !val.empty();
}

bool Model::convert_to_bool(const pt::Space & space)
{
	return space.to_bool();
}




} // namespace

