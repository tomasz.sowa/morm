/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "modelconnector.h"



namespace morm
{

ModelConnector::ModelConnector()
{
	flat_connector = nullptr;
	db_connector = nullptr;
	log = nullptr;

	out_stream = nullptr;
	out_stream_allocated = false;

	clearer = nullptr;
	clearer_allocated = false;
}


ModelConnector::~ModelConnector()
{
	deallocate_stream();
	deallocate_clearer();
}


void ModelConnector::set_logger(pt::Log * log)
{
	this->log = log;
}

void ModelConnector::set_logger(pt::Log & log)
{
	this->log = &log;
}

pt::Log * ModelConnector::get_logger()
{
	return log;
}



void ModelConnector::deallocate_stream()
{
	if( out_stream_allocated )
	{
		delete out_stream;
		out_stream = nullptr;
		out_stream_allocated = false;
	}
}


void ModelConnector::allocate_default_stream()
{
	deallocate_stream();
	out_stream = new pt::TextStream();
	out_stream_allocated = true;
}


void ModelConnector::allocate_default_stream_if_needed()
{
	if( !out_stream )
	{
		allocate_default_stream();
	}
}


void ModelConnector::deallocate_clearer()
{
	if( clearer_allocated )
	{
		delete clearer;
		clearer = nullptr;
		clearer_allocated = false;
	}
}


void ModelConnector::allocate_default_clearer()
{
	deallocate_clearer();
	clearer = new Clearer();
	clearer_allocated = true;
}


void ModelConnector::allocate_default_clearer_if_needed()
{
	if( !clearer )
	{
		allocate_default_clearer();
	}
}


void ModelConnector::set_stream(pt::Stream & stream)
{
	deallocate_stream();
	this->out_stream = &stream;
}


pt::Stream * ModelConnector::get_stream()
{
	allocate_default_stream_if_needed();
	return out_stream;
}



void ModelConnector::set_flat_connector(FlatConnector & flat_connector)
{
	this->flat_connector = &flat_connector;
}


FlatConnector * ModelConnector::get_flat_connector()
{
	return this->flat_connector;
}



void ModelConnector::set_db_connector(DbConnector & db_connector)
{
	this->db_connector = &db_connector;
}


DbConnector * ModelConnector::get_db_connector()
{
	return db_connector;
}


void ModelConnector::set_clearer(Clearer & clearer)
{
	deallocate_clearer();
	this->clearer = &clearer;
}


Clearer * ModelConnector::get_clearer()
{
	allocate_default_clearer_if_needed();
	return this->clearer;
}




}

