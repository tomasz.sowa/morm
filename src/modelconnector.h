/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_modelconnector
#define headerfile_morm_src_modelconnector

#include "clearer.h"
#include "dbconnector.h"
#include "flatconnector.h"
#include "log/log.h"


namespace morm
{
class Model;


template<typename ModelClass>
class Finder;


class ModelConnector
{

public:

	ModelConnector();
	ModelConnector(const ModelConnector &) = delete;
	virtual ~ModelConnector();

	virtual void set_logger(pt::Log * log);
	virtual void set_logger(pt::Log & log);

	virtual pt::Log * get_logger();

	virtual void set_stream(pt::Stream & stream);
	virtual pt::Stream * get_stream();

	virtual void set_flat_connector(FlatConnector & flat_connector);
	virtual FlatConnector * get_flat_connector();

	virtual void set_db_connector(DbConnector & db_connector);
	virtual DbConnector * get_db_connector();

	virtual void set_clearer(Clearer & clearer);
	virtual Clearer * get_clearer();



protected:

	pt::Log * log;

	FlatConnector * flat_connector;
	DbConnector * db_connector;

	pt::Stream * out_stream;
	bool out_stream_allocated;

	Clearer * clearer;
	bool clearer_allocated;

	void deallocate_stream();
	void allocate_default_stream();
	void allocate_default_stream_if_needed();

	void deallocate_clearer();
	void allocate_default_clearer();
	void allocate_default_clearer_if_needed();

};

}

#endif
