/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_modelcontainerwrapper
#define headerfile_morm_src_modelcontainerwrapper

#include <vector>
#include <list>
#include <limits>
#include "baseobjectwrapper.h"


namespace morm
{
class Model;


class ModelContainerWrapper : public BaseObjectWrapper
{
public:

	virtual Model * get_model()
	{
		return nullptr;
	}

	virtual bool is_container_empty()
	{
		return true;
	}

	virtual void increment_iterator()
	{
	}

	virtual bool is_iterator_correct()
	{
		return false;
	}

	virtual void set_iterator_at_first_model()
	{
	}
};



template<typename ContainerType>
class ModelWrapperBaseContainer : public ModelContainerWrapper
{
public:

	ModelWrapperBaseContainer(ContainerType * container)
	{
		this->container = container;
		iterator = container->end();
	}

	bool is_container_empty()
	{
		return container->empty();
	}

	void increment_iterator()
	{
		if( iterator == container->end() )
		{
			iterator = container->begin();
		}
		else
		{
			++iterator;
		}
	}

	bool is_iterator_correct()
	{
		return iterator != container->end();
	}

	void set_iterator_at_first_model()
	{
		iterator = container->begin();
	}


protected:

	ContainerType * container;
	typename ContainerType::iterator iterator;

};







template<typename VectorType>
class ModelWrapperVector : public ModelWrapperBaseContainer<std::vector<VectorType>>
{
public:

	using ModelWrapperBaseContainer<std::vector<VectorType>>::container;
	using ModelWrapperBaseContainer<std::vector<VectorType>>::iterator;

	ModelWrapperVector(std::vector<VectorType> * container) : ModelWrapperBaseContainer<std::vector<VectorType>>(container)
	{
	}

	Model * get_model()
	{
		if( iterator != container->end() )
		{
			return &(*iterator);
		}

		return nullptr;
	}

};




template<typename VectorType>
class ModelWrapperVectorPointer : public ModelWrapperBaseContainer<std::vector<VectorType*>>
{
public:

	using ModelWrapperBaseContainer<std::vector<VectorType*>>::container;
	using ModelWrapperBaseContainer<std::vector<VectorType*>>::iterator;

	ModelWrapperVectorPointer(std::vector<VectorType*> * container) : ModelWrapperBaseContainer<std::vector<VectorType*>>(container)
	{
	}

	Model * get_model()
	{
		if( iterator != container->end() )
		{
			return (*iterator);
		}

		return nullptr;
	}

};





template<typename ListType>
class ModelWrapperList : public ModelWrapperBaseContainer<std::list<ListType>>
{
public:

	using ModelWrapperBaseContainer<std::list<ListType>>::container;
	using ModelWrapperBaseContainer<std::list<ListType>>::iterator;

	ModelWrapperList(std::list<ListType> * container): ModelWrapperBaseContainer<std::list<ListType>>(container)
	{
	}

	Model * get_model()
	{
		if( iterator != container->end() )
		{
			return &(*iterator);
		}

		return nullptr;
	}

};




template<typename ListType>
class ModelWrapperListPointer : public ModelWrapperBaseContainer<std::list<ListType*>>
{
public:

	using ModelWrapperBaseContainer<std::list<ListType*>>::container;
	using ModelWrapperBaseContainer<std::list<ListType*>>::iterator;

	ModelWrapperListPointer(std::list<ListType*> * container): ModelWrapperBaseContainer<std::list<ListType*>>(container)
	{
	}

	Model * get_model()
	{
		if( iterator != container->end() )
		{
			return (*iterator);
		}

		return nullptr;
	}

};




}

#endif
