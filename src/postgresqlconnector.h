/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_postgresqlconnector
#define headerfile_morm_src_postgresqlconnector

#include <libpq-fe.h>
#include <string>
#include "dbconnector.h"
#include "postgresqlqueryresult.h"



namespace morm
{

class PostgreSQLConnector : public DbConnector
{
public:

	PostgreSQLConnector();
	PostgreSQLConnector(const PostgreSQLConnector &) = delete;

	virtual ~PostgreSQLConnector();


	bool query(const pt::Stream & stream, QueryResult & query_result) override;
	bool query(const char * query_str, QueryResult & query_result) override;
	bool query(const std::string & query_str, QueryResult & query_result) override;

	bool query_select(const char * query_str, QueryResult & query_result) override;
	bool query_update(const char * query_str, QueryResult & query_result) override;
	bool query_insert(const char * query_str, QueryResult & query_result) override;
	bool query_remove(const char * query_str, QueryResult & query_result) override;
	bool query_declare_cursor(const char * query_str, QueryResult & query_result) override;

	bool query_select(const pt::Stream & stream, QueryResult & query_result) override;
	bool query_update(const pt::Stream & stream, QueryResult & query_result) override;
	bool query_insert(const pt::Stream & stream, QueryResult & query_result) override;
	bool query_remove(const pt::Stream & stream, QueryResult & query_result) override;
	bool query_declare_cursor(const pt::Stream & stream, QueryResult & query_result) override;

	/*
	 * https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-CONNSTRING
	 */
	virtual void set_conn_param(const std::wstring & database_conn_string);


	/*
	 *
	 * database_host - name of host to connect to (can be empty)
	 * database_hostaddr - numeric IP address of host to connect to (can be empty)
	 *
	 *
	 * meaning of this parameters is the same as described in
	 * https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-CONNSTRING
	 *
	 * from above documentation:
	 * Using hostaddr allows the application to avoid a host name look-up, which might be important
	 * in applications with time constraints. However, a host name is required for GSSAPI or SSPI
	 * authentication methods, as well as for verify-full SSL certificate verification.
	 * The following rules are used:
	 *
	 * - If host is specified without hostaddr, a host name lookup occurs.
	 *
	 * - If hostaddr is specified without host, the value for hostaddr gives the server network address.
	 *   The connection attempt will fail if the authentication method requires a host name.
	 *
	 * - If both host and hostaddr are specified, the value for hostaddr gives the server network address.
	 *   The value for host is ignored unless the authentication method requires it, in which case it will
	 *   be used as the host name.
	 *
	 */
	virtual void set_conn_param(const std::wstring & database_host, const std::wstring & database_hostaddr, const std::wstring & database_port,
								const std::wstring & database, const std::wstring & user, const std::wstring & pass);

	virtual void set_conn_param(const std::wstring & database, const std::wstring & user, const std::wstring & pass);


	/*
	 * waiting for a valid connection to the database
	 *
	 * attempts_max - how many connection attempts are allowed (0 - infinite)
	 * attempt_delay - delay between each attempt (in seconds)
	 *
	 */
	virtual bool wait_for_connection(size_t attempts_max = 0, size_t attempt_delay = 5);


	/*
	 * close the connection with the database if it was open
	 */
	virtual void close();



protected:

	PGconn * pg_conn;
	pt::TextStream stream;
	std::string query_str;

	std::wstring db_conn_string;
	std::wstring db_host;
	std::wstring db_hostaddr;
	std::wstring db_port;
	std::wstring db_database;
	std::wstring db_user;
	std::wstring db_pass;

	virtual void set_db_parameters();
	virtual void log_no_connection(size_t attempts);
	virtual void log_connection_socket();
	virtual bool assert_connection_is_working(bool put_log = true);
	virtual void connect();

	virtual bool do_query(const char * query_str, PostgreSQLQueryResult * psql_result);
	virtual bool query_command(const char * query_str, QueryResult & query_result, ExecStatusType expected_status);
	virtual void allocate_default_expression() override;
	virtual void overwrite(pt::TextStream & stream);
	virtual void overwrite(pt::WTextStream & stream);
	virtual const char * query_last_sequence(const wchar_t * sequence_table_name) override;
	virtual QueryResult * create_query_result() override;

	void log_unsupported_bin_format();

	void unescape_bin_char(const char * str, char & field_value) override;
	void unescape_bin_char(const char * str, char32_t & field_value) override;

	void unescape_bin_string(const char * str, std::string & out) override;
	void unescape_bin_string(const char * str, std::wstring & out) override;
	void unescape_bin_string(const char * str, pt::Stream & out) override;


};

}

#endif
