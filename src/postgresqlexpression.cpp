/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "postgresqlexpression.h"


namespace morm
{


void PostgreSQLExpression::before_field_value_string(const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_binary() )
	{
		(*out_stream) << "'\\x";
	}
	else
	if( field_type.is_hexadecimal() )
	{
		(*out_stream) << "'";
	}
	else
	if( field_type.is_numeric() )
	{
		(*out_stream) << "'";
	}
	else
	{
		(*out_stream) << "E'";

		if( model_env && model_env->add_prefix_percent )
			(*out_stream) << '%';
	}
}


void PostgreSQLExpression::after_field_value_string(const FT & field_type, ModelEnv * model_env)
{
	if( model_env && model_env->add_postfix_percent )
		(*out_stream) << '%';

	(*out_stream) << "'";
}


bool PostgreSQLExpression::esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	if( model_env && model_env->use_escaping_for_like )
	{
		if( val == '%' )
		{
			stream << "\\\\%"; // gives: "\\%" (we are using a string form with E so we need to double backslashes here)
			return true;
		}
		else
		if( val == '_' )
		{
			stream << "\\\\_"; // gives: "\\_"
			return true;
		}
		else
		if( val == '\\' )
		{
			stream << "\\\\\\\\"; // gives: "\\\\"
			return true;
		}
	}

	if( val == '\\' )
	{
		stream << "\\\\";
		return true;
	}
	else
	if( val == '\'' )
	{
		stream << "\\\'"; // don't use "''" because we use the method for PQconnectdb too
		return true;
	}
	else
	if( val == 0 )
	{
		// may put the replacement character to the stream?
		return true;
	}

	return false;
}


DbExpression & PostgreSQLExpression::page(pt::Stream & stream, size_t page_number, size_t page_size)
{
	stream << " OFFSET " << (page_number*page_size) << " LIMIT " << page_size << " ";
	return *this;
}


}
