/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2018-2019, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_queryresult
#define headerfile_morm_src_queryresult

#include <log/log.h>
#include <string>


namespace morm
{

struct QueryResult
{
	bool status;
	size_t result_rows; // how many rows in the result query
	size_t result_cols; // how many columns in the result query
	size_t cur_row;		// used for reading
	std::wstring error_msg;

	std::string temp_column_name;
	size_t references_count;

	QueryResult();
	virtual ~QueryResult();

	virtual void clear();
	virtual bool has_db_result();

	virtual const char * get_field_string_value(int column_index);
	virtual const char * get_field_string_value(const char * column_name);
	virtual const char * get_field_string_value(const wchar_t * field_name);

	/*
	 * returns -1 if there is no such a column
	 *
	 */
	virtual int get_column_index(const char * column_name);
	virtual int get_column_index(const wchar_t * column_name);
	virtual bool is_null(int column_index);

	// may it should be changed to size_t?
	virtual const char * get_value_from_result(int row, int col);
	virtual int          get_value_length(int row, int col);
	virtual bool         is_null(int row, int col);

	virtual void         dump_column_names(pt::Log & log);
};



} // namespace

#endif
