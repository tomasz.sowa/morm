/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2023, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_select
#define headerfile_morm_src_select

namespace morm
{

/*
 * additional arguments for Finder::select method
 */
class Select
{
public:

	enum SelectType
	{
		default_type = 0,
		no_auto_generated_columns = 1,
		with_rows_counter = 2,
		distinct = 4,
	};


	/*
	 * type can be a superposition from SelectType values
	 */
	int type;



	Select()
	{
		type = 0;
	}

	Select(const Select & select_type)
	{
		this->type = select_type.type;
	}

	Select(SelectType select_type)
	{
		this->type = static_cast<int>(select_type);
	}

	Select(int type)
	{
		this->type = type;
	}

	Select & operator=(const Select & select_type)
	{
		type = select_type.type;
		return *this;
	}

	Select & operator=(SelectType select_type)
	{
		this->type = static_cast<int>(select_type);
		return *this;
	}

	Select & operator=(int type)
	{
		this->type = type;
		return *this;
	}


	bool is_flag_set(int flag_mask) const
	{
		return (type & flag_mask) != 0;
	}


	bool is_with_rows_counter() const
	{
		return is_flag_set(with_rows_counter);
	}


	bool is_no_auto_generated_columns() const
	{
		return is_flag_set(no_auto_generated_columns);
	}


	bool is_distinct() const
	{
		return is_flag_set(distinct);
	}

};

}

#endif
