/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_spacewrapper
#define headerfile_morm_src_spacewrapper

#include <vector>
#include <limits>
#include "space/space.h"
#include "baseobjectwrapper.h"


namespace morm
{


class SpaceWrapper : public BaseObjectWrapper
{
public:

	SpaceWrapper(pt::Space * space)
	{
		this->space = space;
		initialize_indices();
	}

	pt::Space * get_space()
	{
		return space;
	}

	size_t space_indices_table_size()
	{
		return SPACE_INDICES_TABLE_SIZE;
	}


	void increment_iterator(size_t space_index, size_t table_size)
	{
		if( space_index < indices.size() )
		{
			if( indices[space_index] >= table_size )
			{
				indices[space_index] = 0;
			}
			else
			{
				indices[space_index] += 1;
			}
		}
	}

	void invalidate_iterators(size_t space_index_start)
	{
		for(size_t i = space_index_start ; i < indices.size() ; ++i)
		{
			indices[i] = std::numeric_limits<size_t>::max();
		}
	}

	size_t get_space_iterator_value(size_t space_index)
	{
		if( space_index < indices.size() )
		{
			return indices[space_index];
		}

		return std::numeric_limits<size_t>::max();
	}

protected:

	const size_t SPACE_INDICES_TABLE_SIZE = 32;
	pt::Space * space;
	std::vector<size_t> indices;


	void initialize_indices()
	{
		indices.resize(SPACE_INDICES_TABLE_SIZE);
		invalidate_iterators(0);
	}

};


}

#endif
