/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "transaction.h"
#include "dbconnector.h"


namespace morm
{


Transaction::Transaction(ModelConnector * model_connector, bool auto_begin_transaction)
{
	this->model_connector = model_connector;
	is_transaction_started = false;
	is_transaction_successful = true;
	transaction_index = 0;
	transaction_group = 0;

	if( auto_begin_transaction )
	{
		begin();
	}
}


Transaction::Transaction(const Transaction &)
{
	// at the moment do not allow to copy transactions (make me private)
}

Transaction::Transaction(Transaction &&)
{
	// at the moment do not allow to move transactions (make me private)
}



Transaction::~Transaction()
{
	if( is_current_group() && is_transaction_started )
	{
		rollback();
	}
}



bool Transaction::is_started()
{
	return is_current_group() && is_transaction_started;
}


bool Transaction::is_successful()
{
	return is_transaction_successful;
}


void Transaction::set_successful(bool is_successful)
{
	is_transaction_successful = is_successful;
}





bool Transaction::begin()
{
	bool status = false;
	pt::Log * log = get_logger();

	if( is_current_group() && is_transaction_started )
	{
		if( log )
		{
			(*log) << pt::Log::log2 << "Morm: a transaction is already started - rollbacking existing transaction before creating a new one" << pt::Log::logend;
		}

		rollback(); // what if there is an error here? skip it at the moment
	}

	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			status = db_connector->begin();
			transaction_index = db_connector->get_transaction_index();
			transaction_group = db_connector->get_transaction_group();
		}
	}

	if( status )
	{
		is_transaction_started = true;
		is_transaction_successful = true;
	}
	else
	{
		is_transaction_started = false;
		is_transaction_successful = false;
	}

	return status;
}


bool Transaction::begin_if_needed()
{
	bool status = true;

	if( !is_transaction_started )
	{
		status = begin();
	}

	return status;
}


bool Transaction::rollback()
{
	bool status = false;
	pt::Log * log = get_logger();

	if( is_current_group() )
	{
		if( is_transaction_started )
		{
			if( model_connector )
			{
				DbConnector * db_connector = model_connector->get_db_connector();

				if( db_connector )
				{
					status = db_connector->rollback(transaction_index);
				}
			}

			// set it to false even if rollback failed
			is_transaction_started = false;
		}
		else
		{
			if( log )
			{
				(*log) << pt::Log::log1 << "Morm: not doing rollback, a transaction was not started - skipping" << pt::Log::logend;
			}
		}
	}
	else
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: not doing rollback, this transaction has already been completed" << pt::Log::logend;
		}
	}

	return status;
}


bool Transaction::commit()
{
	bool status = false;
	pt::Log * log = get_logger();

	if( is_current_group() )
	{
		if( is_transaction_started )
		{
			if( model_connector )
			{
				DbConnector * db_connector = model_connector->get_db_connector();

				if( db_connector )
				{
					status = db_connector->commit(transaction_index);
				}
			}

			// set it to false even if commit failed
			is_transaction_started = false;
		}
		else
		{
			if( log )
			{
				(*log) << pt::Log::log1 << "Morm: not doing commit, a transaction was not started - skipping" << pt::Log::logend;
			}
		}
	}
	else
	{
		if( log )
		{
			(*log) << pt::Log::log1 << "Morm: not doing commit, this transaction has already been completed" << pt::Log::logend;
		}
	}

	return status;
}


bool Transaction::finish()
{
	bool status = false;

	if( is_transaction_successful )
		status = commit();
	else
		status = rollback();

	return status;
}


pt::Log * Transaction::get_logger()
{
	pt::Log * logger = nullptr;

	if( model_connector )
	{
		logger = model_connector->get_logger();
	}

	return logger;
}


bool Transaction::is_current_group()
{
	if( model_connector )
	{
		DbConnector * db_connector = model_connector->get_db_connector();

		if( db_connector )
		{
			return (db_connector->get_transaction_group() == transaction_group);
		}
	}

	return false;
}



} // namespace


