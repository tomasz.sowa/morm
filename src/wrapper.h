/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2021, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_wrapper
#define headerfile_morm_src_wrapper

#include "spacewrapper.h"
#include "date/date.h"
#include "modelcontainerwrapper.h"


namespace morm
{


class Wrapper
{
public:

	Model * model;
	ModelContainerWrapper * model_container_wrapper;
	pt::Date * date;
	SpaceWrapper * space_wrapper;


	Wrapper()
	{
		model = nullptr;
		model_container_wrapper = nullptr;
		date = nullptr;
		space_wrapper = nullptr;
	}


	Wrapper(const Wrapper & wrapper)
	{
		model = nullptr;
		model_container_wrapper = nullptr;
		date = nullptr;
		space_wrapper = nullptr;

		copy(wrapper);
	}


	Wrapper & operator=(const Wrapper & wrapper)
	{
		clear();
		copy(wrapper);

		return *this;
	}


	virtual ~Wrapper()
	{
		clear();
	}


	virtual void clear()
	{
		if( model_container_wrapper )
		{
			model_container_wrapper->decrement_reference_counter();

			if( model_container_wrapper->get_reference_counter() == 0 && model_container_wrapper->should_be_auto_removed() )
			{
				delete model_container_wrapper;
			}
		}

		if( space_wrapper )
		{
			space_wrapper->decrement_reference_counter();

			if( space_wrapper->get_reference_counter() == 0 && space_wrapper->should_be_auto_removed() )
			{
				delete space_wrapper;
			}
		}

		model = nullptr;
		model_container_wrapper = nullptr;
		date =  nullptr;
		space_wrapper = nullptr;

		clear_childs();
	}


	virtual void clear_childs()
	{
		childs_map.clear();
	}


	virtual Wrapper * add_child(const std::wstring & child_name, Wrapper & wrapper)
	{
		Wrapper & w = childs_map[child_name];
		w.clear();
		w = wrapper;
		return &w;
	}


	virtual Wrapper * find_child(const std::wstring & child_name)
	{
		auto i = childs_map.find(child_name);

		if( i != childs_map.end() )
		{
			return &i->second;
		}

		return nullptr;
	}


	bool has_object()
	{
		return model || model_container_wrapper || date || space_wrapper;
	}


	bool has_model_object()
	{
		return model || model_container_wrapper;
	}


private:

	std::map<std::wstring, Wrapper> childs_map;


	void copy(const Wrapper & wrapper)
	{
		model = wrapper.model;
		model_container_wrapper = wrapper.model_container_wrapper;
		date = wrapper.date;
		space_wrapper = wrapper.space_wrapper;

		if( model_container_wrapper )
		{
			model_container_wrapper->increment_reference_counter();
		}

		if( space_wrapper )
		{
			space_wrapper->increment_reference_counter();
		}

		// childs_map don't need to be copied
	}
};



}

#endif
