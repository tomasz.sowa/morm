/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "xmlconnector.h"
#include "xmlexpression.h"


namespace morm
{


XMLConnector::XMLConnector()
{
	put_doctype = true;
	put_root_element = true;
}



void XMLConnector::set_putting_doctype(bool put_doctype)
{
	this->put_doctype = put_doctype;
}


void XMLConnector::set_putting_root_element(bool put_root_element)
{
	this->put_root_element = put_root_element;
}


bool XMLConnector::get_putting_doctype()
{
	return put_doctype;
}


bool XMLConnector::get_putting_root_element()
{
	return put_root_element;
}


void XMLConnector::set_root_element(const wchar_t * root_name)
{
	root_element_name = root_name;
}


void XMLConnector::set_root_element(const std::wstring & root_name)
{
	root_element_name = root_name;
}


std::wstring & XMLConnector::get_root_element()
{
	return root_element_name;
}



void XMLConnector::allocate_default_expression()
{
	deallocate_expression();
	flat_expression = new XMLExpression();
	expression_allocated = true;

}


void XMLConnector::to_text(pt::Stream & stream, Model & model, Export exp)
{
	allocate_default_expression_if_needed();

	if( flat_expression )
	{
		flat_expression->clear();
		flat_expression->set_work_mode(MORM_WORK_MODE_MODEL_FIELDS_VALUES_FIELDS);
		flat_expression->allow_to_use_prefix(false);

		put_doctype_definition(stream);
		put_opening_root_element(stream);
		flat_expression->generate_from_model(stream, model);
		put_closing_root_element(stream);
	}
}


void XMLConnector::put_doctype_definition(pt::Stream & stream)
{
	if( put_doctype )
	{
		stream << L"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	}
}


void XMLConnector::put_opening_root_element(pt::Stream & stream)
{
	if( put_root_element )
	{
		stream << L"<";
		put_root_element_name(stream);
		stream << L">";
	}
}


void XMLConnector::put_closing_root_element(pt::Stream & stream)
{
	if( put_root_element )
	{
		stream << L"</";
		put_root_element_name(stream);
		stream << L">";
	}
}


void XMLConnector::put_root_element_name(pt::Stream & stream)
{
	if( root_element_name.empty() )
	{
		stream << L"xml";
	}
	else
	{
		if( flat_expression )
		{
			flat_expression->esc(root_element_name, stream, FT::default_type, nullptr);
		}
	}
}


}
