/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_xmlconnector
#define headerfile_morm_src_xmlconnector

#include "flatconnector.h"


namespace morm
{

class XMLConnector : public FlatConnector
{

public:

	XMLConnector();

	virtual void set_putting_doctype(bool put_doctype);
	virtual void set_putting_root_element(bool put_root_element);
	virtual bool get_putting_doctype();
	virtual bool get_putting_root_element();

	virtual void set_root_element(const wchar_t * root_name);
	virtual void set_root_element(const std::wstring & root_name);
	virtual std::wstring & get_root_element();

	void to_text(pt::Stream & stream, Model & model, Export exp) override;


protected:

	bool put_doctype;
	bool put_root_element;
	std::wstring root_element_name;

	void allocate_default_expression() override;

	virtual void put_doctype_definition(pt::Stream & stream);
	virtual void put_opening_root_element(pt::Stream & stream);
	virtual void put_closing_root_element(pt::Stream & stream);
	virtual void put_root_element_name(pt::Stream & stream);

};

}

#endif
