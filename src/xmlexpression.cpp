/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "xmlexpression.h"
#include "morm_types.h"
#include "convert/misc.h"


namespace morm
{


void XMLExpression::before_field_name()
{
	(*out_stream) << "<";
}


void XMLExpression::after_field_name()
{
	(*out_stream) << ">";
}



void XMLExpression::before_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_space() )
	{
		//before_field_value_string(field_type, model_env);
	}
}

void XMLExpression::after_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_space() )
	{
		//after_field_value_string(field_type, model_env);
	}
}


void XMLExpression::put_field_closing_name(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env)
{
	if( field_type.is_raw_field_name() )
	{
		(*out_stream) << '/';
		(*out_stream) << field_name;
	}
	else
	{
		before_field_name();
		(*out_stream) << '/';
		esc(field_name, *out_stream, FT::default_type, nullptr); /* do not use provided field_type here - it would use e.g. binary mode if it was set, similar don't use model_env */
		after_field_name();
	}
}


void XMLExpression::put_name_value_separator()
{
}


bool XMLExpression::esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	return pt::try_esc_to_xml(val, stream);
}


void XMLExpression::esc(const pt::Space & space, pt::Stream & stream, const FT & field_type, ModelEnv * model_env)
{
	bool pretty_print = field_type.is_pretty_print();

	if( field_type.is_space() )
	{
		pt::WTextStream tmp_stream;
		space.serialize_to_space_stream(tmp_stream, pretty_print);
		BaseExpression::esc(tmp_stream, stream, field_type, model_env);
	}
	else
	{
		/*
		 * IMPROVEME it would be better to serialize to xml
		 *
		 */
		pt::WTextStream tmp_stream;
		space.serialize_to_json_stream(tmp_stream, pretty_print);
		BaseExpression::esc(tmp_stream, stream, field_type, model_env);
	}
}


void XMLExpression::put_value_list_opening_index(size_t index, const FT & field_type)
{
	(*out_stream) << L"<item index=\"" << index << L"\">";
}


void XMLExpression::put_value_list_closing_index(size_t index, const FT & field_type)
{
	(*out_stream) << L"</item>";
}


void XMLExpression::field_value_list_separator()
{
}


}
