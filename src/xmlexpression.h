/*
 * This file is a part of morm
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/*
 * Copyright (c) 2022-2024, Tomasz Sowa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_morm_src_xmlexpression
#define headerfile_morm_src_xmlexpression

#include "flatexpression.h"



namespace morm
{

class XMLExpression : public FlatExpression
{

protected:

	void put_field_closing_name(const wchar_t * field_name, const FT & field_type, ModelEnv * model_env) override;

	void before_field_name() override;
	void after_field_name() override;

	void put_name_value_separator() override;

	using BaseExpression::esc;
	bool esc_char(char32_t val, pt::Stream & stream, const FT & field_type, ModelEnv * model_env) override;
	void esc(const pt::Space & space, pt::Stream & stream, const FT & field_type, ModelEnv * model_env) override;


private:

	void before_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env) override;
	void after_field_value(const pt::Space &, const FT & field_type, ModelEnv * model_env) override;

	void put_value_list_opening_index(size_t index, const FT & field_type) override;
	void put_value_list_closing_index(size_t index, const FT & field_type) override;

	void field_value_list_separator() override;

};

}

#endif
